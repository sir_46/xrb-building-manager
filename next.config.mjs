/** @type {import('next').NextConfig} */
const nextConfig = {
  //   env: {
  //     PUBLIC_BKNEXT_URL: 'https://accounts.bitkubnext.com',
  //     PUBLIC_BKNEXT_ACCESS_TOKEN_URL:
  //       'https://accounts.bitkubnext.com/api/oauth2/access_token',
  //     PUBLIC_BKNEXT_ACCOUNT_API_URL: 'https://accounts.bitkubnext.com/api',
  //     PUBLIC_BKNEXT_API_URL: 'https://api.bitkubnext.io',
  //     BK_CLIENT_ID: '63a524dbb2c64740a13199cc',
  //     BK_CLIENT_SECRET: 'wRB6wd0iivUVbesSRbymEJ5SfnbLQziQ',
  //     PUBLIC_BK_REDIRECT_URL: 'http://localhost:3003/oauth/callback',
  //   },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'www.sandbox.game',
        port: '',
        pathname: '/cdn-cgi/**',
      },
      {
        protocol: 'https',
        hostname: 'xrbservicedev.themonkgames.dev',
        port: '',
        pathname: '/building-manager/**',
      },
      {
        protocol: 'https',
        hostname: 'www.xrbgalaxy.com',
        port: '',
        pathname: '/assets/**',
      },
      {
        protocol: 'https',
        hostname: 'xrbservicedev.themonkgames.dev',
        port: '',
        pathname: '/assets/**',
      },
      // bitkubnext
      {
        protocol: 'https',
        hostname: 'static.bitkubnext.com',
        pathname: '/nft/**',
      },
      {
        protocol: 'https',
        hostname: 'static.bitkubnext.com',
        pathname: '/bitkub-next/**',
      },
      {
        protocol: 'https',
        hostname: 'static.bitkubnext.com',
        pathname: '/accounts/**',
      },
      // bitkubnft
      {
        protocol: 'https',
        hostname: 'static.bitkubnft.com',
        pathname: '/studio/**',
      },
      {
        protocol: 'https',
        hostname: 'static.bitkubnft.com',
        pathname: '/backoffice/**',
      },
      // NFT
      {
        protocol: 'https',
        hostname: 'ipfs.bitkubnext.io',
        pathname: '/ipfs/**',
      },
      {
        protocol: 'https',
        hostname: 'bitkubipfs.io',
        pathname: '/ipfs/**',
      },
      {
        protocol: 'https',
        hostname: 'ipfs.morningmoonvillage.com',
        pathname: '/ipfs/**',
      },
      {
        protocol: 'https',
        hostname: 'game.bitkubmetaverse.com',
      },
      {
        protocol: 'https',
        hostname: 'xrbservicedev.themonkgames.dev',
      },
    ],
  },
};

export default nextConfig;
