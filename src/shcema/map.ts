import { z } from 'zod';
export const MapFilterSchema = z.object({
  area: z.string(),
  landType: z.string(),
  size: z.enum(['xs', 's', 'm', 'l', 'xl']),
});

export type MapFilterSchemaType = z.infer<typeof MapFilterSchema>;

const nonempty = z
  .string()
  .transform((t) => t?.trim())
  .pipe(z.string().min(1));

export const MapLandBuildingSchema = z.object({
  building: z
    .string({
      required_error: 'Please select building.',
    })
    .pipe(nonempty),
});

export type MapLandBuildingSchemaType = z.infer<typeof MapLandBuildingSchema>;

export const LandTypeShema = z.object({
  items: z.array(z.string()).refine((value) => value.some((item) => item), {
    message: 'You have to select at least one item.',
  }),
});

export type LandTypeShemaType = z.infer<typeof LandTypeShema>;

export const LandSizeShema = z.object({
  items: z.array(z.string()).refine((value) => value.some((item) => item), {
    message: 'You have to select at least one item.',
  }),
});
export type LandSizeShemaType = z.infer<typeof LandSizeShema>;
