import { Icons } from '@/components/icons';
import Image from 'next/image';
import { redirect } from 'next/navigation';
import { getServerSession } from 'next-auth';
import { authOptions } from '@/lib/auth';
import BKNextButton from '@/components/bknext-button';

export default async function Home() {
  const session = await getServerSession(authOptions);

  if (session) {
    redirect('/map');
  }

  return (
    <div className='container flex h-svh flex-col items-center justify-center  py-20 md:p-6'>
      <Icons.Star className='absolute left-[44%] top-2/4 z-50' />
      <div className='flex flex-col md:flex-row items-center gap-8'>
        <div className='flex-1 flex items-center md:items-start flex-col gap-6'>
          <article className='text-4xl md:text-5xl lg:text-6xl'>
            <p className='font-black text-center  md:text-start'>
              Design Your Own <span className='text-blue-600'>Building</span>{' '}
              Space in Your Preferred Style
            </p>
          </article>
          <div className='row-start-2'>
            <BKNextButton />
          </div>
        </div>
        <div className='flex-1 w-full flex max-w-[400px] relative aspect-square p-8'>
          <Image
            src='/wh-5.jpeg'
            priority
            width={380}
            height={460}
            className='object-cover z-20 m-auto rounded-3xl'
            alt='ENGAGE WITH THE COMMUNITY'
          />
          <Image
            src='/wh-4.jpeg'
            width={380}
            height={460}
            className='absolute z-0 top-0 left-0 object-cover rounded-3xl w-3/4'
            alt='MINI-GAMES AND EVENTS'
          />
          <Image
            src='/wh-3.jpeg'
            width={380}
            height={460}
            className='absolute z-0 bottom-0 right-0 object-cover rounded-3xl w-3/4'
            alt='COLLECT AND TRADE DIGITAL ASSETS'
          />
        </div>
      </div>
    </div>
  );
}
