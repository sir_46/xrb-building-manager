import NFTTokenList from '@/components/nft-token-list';
import ShowNFTCheckbox from '@/components/show-nft-checkbox';

export default function Page() {
  return (
    <div className='relative w-screen flex items-center justify-center'>
      <div className='container'>
        <h1 className='text-3xl font-bold mt-20'>Add NFTs</h1>
        <ShowNFTCheckbox />
        <NFTTokenList className='mt-4' />
      </div>
    </div>
  );
}
