import NFTList from '@/components/nft-list';

interface PageProps {
  params: { address: string };
}
export default function Page({ params }: PageProps) {
  return (
    <div>
      <div className='container'>
        <NFTList className='mt-10' />
      </div>
    </div>
  );
}
