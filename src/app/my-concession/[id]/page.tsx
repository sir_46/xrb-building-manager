import MyConcessionDetail from '@/components/my-concession-detail';

interface PageProps {
  params: { slug: string };
}

export default async function Page({ params }: PageProps) {
  return (
    <div className='relative w-screen flex items-center justify-center'>
      <div className='container'>
        <div className='space-y-2 mt-6'>
          <MyConcessionDetail />
        </div>
      </div>
    </div>
  );
}
