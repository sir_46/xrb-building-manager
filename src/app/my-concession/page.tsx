import MyConcessionBuildingFilter from '@/components/my-concession-building-filter';
import MyConcessionBuildingList from '@/components/my-concession-building-list';
import { Suspense } from 'react';

export default async function Page() {
  return (
    <div className='relative w-screen flex items-center justify-center'>
      <div className='container'>
        <h1 className='mt-20 text-3xl font-bold'>My Concession and Building</h1>
        <Suspense>
          <MyConcessionBuildingFilter className='mt-3' />
          <MyConcessionBuildingList className='mt-3' />
        </Suspense>
      </div>
    </div>
  );
}
