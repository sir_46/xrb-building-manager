import { Suspense } from 'react';
import CallbackClient from '@/components/callback-client';

export default function OAuthCallback() {
  return (
    <Suspense>
      <CallbackClient />
    </Suspense>
  );
}
