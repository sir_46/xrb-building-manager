'use client';
import Image from 'next/image';

import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import livingApi from '@/services/livingApi';
import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import { Button } from '@/components/ui/button';
import LivingCanvas from '@/components/living-canvas';

const LANDS = [
  {
    size: 'xs',
    width: 15,
    height: 15,
  },
  {
    size: 's',
    width: 30,
    height: 15,
  },
  {
    size: 'm',
    width: 45,
    height: 30,
  },
  {
    size: 'l',
    width: 60,
    height: 90,
  },
  {
    size: 'xl',
    width: 180,
    height: 120,
  },
];

export default function Page() {
  const { data: livings } = useQuery({
    queryKey: ['living'],
    queryFn: livingApi.fetchLivingList,
  });

  const [landSize, setLandSize] = useState<string | null>('xs');
  const [image, setImage] = useState<string | undefined>(undefined);
  const [imageSize, setImageSize] = useState<{ w: number; d: number } | null>(
    null
  );

  return (
    <div className='flex flex-col items-center mt-6'>
      <div className='flex space-x-2'>
        {LANDS.map((land) => (
          <Button
            key={land.size}
            size='icon'
            variant={landSize === land.size ? 'default' : 'outline'}
            className='uppercase'
            onClick={() => {
              setLandSize(land.size);
            }}
          >
            {land.size}
          </Button>
        ))}
      </div>
      <Select
        onValueChange={(value) => {
          const finded = livings?.find((lv) => lv.id === Number(value));
          setImage(`${process.env.NEXT_PUBLIC_IMAGE_HOST}${finded?.icon_path}`);
          setImageSize({ w: finded?.w || 0, d: finded?.d || 0 });
        }}
      >
        <SelectTrigger className='w-[180px] mt-4'>
          <SelectValue placeholder='Select a living' />
        </SelectTrigger>
        <SelectContent>
          <SelectGroup>
            {livings?.map((lv) => (
              <SelectItem key={lv.id} value={String(lv.id)}>
                <div className='flex flex-row items-center space-x-2'>
                  <Image
                    src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${lv.icon_path}`}
                    width={24}
                    height={24}
                    alt={lv.name_en}
                  />
                  <p>{lv.name_en}</p>
                </div>
              </SelectItem>
            ))}
          </SelectGroup>
        </SelectContent>
      </Select>
      <LivingCanvas
        key='living-canvas'
        width={LANDS.find((l) => l.size === landSize)?.width || 0}
        height={LANDS.find((l) => l.size === landSize)?.height || 0}
        gridSize={1}
        imageSrc={image}
        livingWidth={imageSize?.w || 0}
        livingHeight={imageSize?.d || 0}
      />
    </div>
  );
}
