import MapFilterButton from '@/components/map-filter-button';
import MapSearchButton from '@/components/map-search-button';
import { Loader } from 'lucide-react';
import dynamic from 'next/dynamic';
import { Suspense } from 'react';

const DynamicMap = dynamic(() => import('@/components/map-swicther'), {
  loading: () => (
    <div className='flex items-center justify-center h-[calc(100svh-64px)]'>
      <Loader className='animate-spin' />
    </div>
  ),
  ssr: false,
});

export default function Map({}) {
  return (
    <div className='relative flex items-center justify-center overflow-hidden'>
      <div className='flex items-center justify-center z-0 relative'>
        <Suspense>
          <DynamicMap />
        </Suspense>
      </div>
      <Suspense>
        <MapFilterButton />
        <MapSearchButton />
      </Suspense>
    </div>
  );
}
