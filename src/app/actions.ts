'use server';

import { cookies } from 'next/headers';

export async function createAuth({
  access_token,
  refresh_token,
  expires_in,
}: {
  access_token: string;
  refresh_token: string;
  expires_in: number;
}) {
  cookies().set('access_token', access_token);
  cookies().set('refresh_token', refresh_token);
  cookies().set('expires_in', String(expires_in));
}

export async function removeAuth() {
  cookies().delete('access_token');
  cookies().delete('refresh_token');
  cookies().delete('expires_in');
}
