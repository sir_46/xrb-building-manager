import { useEffect } from 'react';

function useLocalStorageChange(
  key: string,
  callback: (newValue: string | null) => void
) {
  useEffect(() => {
    // Define the event handler
    const handleLocalStorageChange = (event: StorageEvent) => {
      if (event.key === key) {
        callback(event.newValue);
      }
    };

    // Add the event listener for local storage changes
    window.addEventListener('storage', handleLocalStorageChange);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('storage', handleLocalStorageChange);
    };
  }, [key, callback]);
}

export default useLocalStorageChange;
