import buildingApi from '@/services/buildingApi';
import { IBuilding } from '@/types';
import { useQuery } from '@tanstack/react-query';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { ImageOverlay, LayerGroup } from 'react-leaflet';
import { rectangleToGeoJSONPolygon, rotatePoint } from './bk-city';
import { getTopviewPath } from '@/lib/utils';
import L, { LatLngBoundsExpression } from 'leaflet';
import BuildingDialogInfo from './building-dialog-info';
import { useSession } from 'next-auth/react';

type Props = {
  transformX?: number;
  transformY?: number;
};

const MyBuildingOverlay = ({ transformX = 0, transformY = 0 }: Props) => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const pathname = usePathname();

  const { data: session } = useSession();
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const { data: mybuilding } = useQuery({
    queryKey: ['my-building', session?.user?.wallet_address, city],
    queryFn: buildingApi.fetchPlacedBuildings,
    select(data) {
      return data.filter(
        (building: IBuilding) =>
          building.city === (city === 'bkcity' ? 'BKCITY' : 'DUNCITY')
      );
    },
    enabled: !!session?.user?.wallet_address,
  });

  const RenderBuildings = useCallback(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];

    const features = mybuilding
      ?.filter(
        (land) => sizes?.includes(land.size.toLowerCase()) // filter by size
      )
      .map((building) => {
        return rectangleToGeoJSONPolygon(
          building.pos_x + transformX,
          building.pos_y - transformY,
          building.w,
          building.d,
          building.token_id
        );
      });

    return features?.map((feature: any, idx: number) => {
      const bound: LatLngBoundsExpression = [
        [
          feature.geometry.coordinates[0][2][1],
          feature.geometry.coordinates[0][0][0],
        ],
        [
          feature.geometry.coordinates[0][1][1],
          feature.geometry.coordinates[0][1][0],
        ],
      ];

      // Rotate points
      const angle = mybuilding![idx].rot_z;
      // Create a LatLngBounds object
      const latLngBounds = L.latLngBounds(bound);
      const rotatedPoints = bound.map((point) =>
        rotatePoint(
          L.latLng(point[0], point[1]),
          angle,
          latLngBounds.getCenter()
        )
      );

      const rotatedBounds: LatLngBoundsExpression = [
        [rotatedPoints[0].lat, rotatedPoints[0].lng],
        [rotatedPoints[1].lat, rotatedPoints[1].lng],
      ];

      const imageURL = `${process.env.NEXT_PUBLIC_IMAGE_HOST}${
        mybuilding![idx][getTopviewPath(angle)]
      }`;

      return (
        <ImageOverlay
          key={`my-${city}-building-${idx}-${+new Date()}`}
          url={imageURL}
          errorOverlayUrl={`${
            process.env.NEXT_PUBLIC_HOST
          }/building/default-${mybuilding![idx].size.toLowerCase()}.png`}
          bounds={rotatedBounds}
          interactive={true}
          eventHandlers={{
            click: () => {
              const tokenId = mybuilding![idx].token_id;
              const params = new URLSearchParams(searchParams);

              const isMyBuilding =
                session?.user?.wallet_address ===
                mybuilding![idx].wallet_address;

              params.set(
                !isMyBuilding ? 'building-id' : 'my-building-id',
                `${tokenId}`
              );
              router.push(`${pathname}?${params}`);
            },
          }}
        />
      );
    });
  }, [
    mybuilding,
    router,
    pathname,
    searchParams,
    transformX,
    transformY,
    city,
    session,
  ]);
  return (
    <>
      <BuildingDialogInfo />
      <LayerGroup>{RenderBuildings()}</LayerGroup>
    </>
  );
};

export default MyBuildingOverlay;
