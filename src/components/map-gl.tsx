'use client';
import { TileLayer } from 'react-leaflet';
import Map, {
  FullscreenControl,
  GeolocateControl,
  Layer,
  NavigationControl,
  ScaleControl,
} from 'react-map-gl';

type Props = {};

const MapGL = (props: Props) => {
  const conter = [0, 0];

  function ControlPanel() {
    return (
      <div className='control-panel'>
        <h3>Marker, Popup, NavigationControl and FullscreenControl </h3>
        <p>
          Map showing top 20 most populated cities of the United States. Click
          on a marker to learn more.
        </p>
        <p>
          Data source:{' '}
          <a href='https://en.wikipedia.org/wiki/List_of_United_States_cities_by_population'>
            Wikipedia
          </a>
        </p>
        <div className='source-link'>
          <a
            href='https://github.com/visgl/react-map-gl/tree/7.1-release/examples/controls'
            target='_new'
          >
            View Code ↗
          </a>
        </div>
      </div>
    );
  }
  return (
    <>
      <Map
        mapboxAccessToken={process.env.NEXT_PUBLIC_MAP_BOX_TOKEN}
        initialViewState={{
          longitude: conter[0],
          latitude: conter[1],
          zoom: 14,
          bearing: 0,
          pitch: 0,
        }}
        //   style={{ width: '100%', height: 'calc(100vh - 64px)' }}
        // style={{ width: 400, height: 400 }}
        mapStyle='mapbox://styles/mapbox/streets-v9'
      >
        {/* <Layer /> */}
        <ScaleControl position='bottom-right' />
        <FullscreenControl position='bottom-left' />
        <NavigationControl position='bottom-left' />
      </Map>
      {/* <ControlPanel /> */}
    </>
  );
};

export default MapGL;
