'use client';
import { useCallback, useMemo, useRef, useState } from 'react';
import { Button } from './ui/button';
import { ChevronRight, Search } from 'lucide-react';
import { Input } from './ui/input';
import { cn } from '@/lib/utils';
import { useOnClickOutside } from 'usehooks-ts';
import { useMediaQuery } from '@/hooks/useMediaQuery';
import {
  Drawer,
  DrawerContent,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from './ui/drawer';
import { ScrollArea } from './ui/scroll-area';
import { useQuery } from '@tanstack/react-query';
import { Address } from 'viem';
import buildingApi from '@/services/buildingApi';
import Image from 'next/image';
import { useSearchParams } from 'next/navigation';
import { IConcession, ILiving } from '@/types';
import { useSession } from 'next-auth/react';

type Props = {};

const MapSearchButton = (props: Props) => {
  const ref = useRef(null);
  const [open, setOpen] = useState<boolean>(false);
  const [searchText, setSearchText] = useState<string>('');
  const { data: session } = useSession();
  const isDesktop = useMediaQuery('(min-width: 768px)');
  useOnClickOutside(ref, () => setOpen(false));

  const searchParams = useSearchParams();

  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const toggle = useCallback(() => setOpen((prev) => !prev), []);

  const { data } = useQuery({
    queryKey: ['my-buildings', session?.user?.wallet_address, city],
    queryFn: () =>
      buildingApi.fetchMyBuildings({
        city: 'bkcity',
        walletAddress: session?.user?.wallet_address as Address,
      }),
    enabled: !!session?.user?.wallet_address,
  });

  const TrigerButton = useCallback(() => {
    return (
      <Button
        size='icon'
        variant='outline'
        className='rounded-full'
        onClick={toggle}
      >
        {open ? (
          <ChevronRight className='size-5' />
        ) : (
          <Search className='size-5' />
        )}
      </Button>
    );
  }, [toggle, open]);

  const combineConcessionLiving = useMemo(() => {
    const searchConcessioon: Array<IConcession> =
      data?.concession?.filter((concession) =>
        concession.name_en.toLowerCase().includes(searchText)
      ) || [];

    const searchLiving: Array<ILiving> =
      data?.living?.filter((concession) =>
        concession.name_en.toLowerCase().includes(searchText)
      ) || [];
    return [...searchConcessioon, ...searchLiving];
  }, [searchText, data]);

  if (isDesktop) {
    return (
      <div
        ref={ref}
        className={cn(
          'absolute top-8 transition-all',
          open ? 'right-4' : '-right-80'
        )}
      >
        <div className='flex gap-2'>
          <TrigerButton />

          <div className={cn('w-80 rounded bg-card px-4 py-5')}>
            <Input
              placeholder='Search'
              className='z-20 bg-card'
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
            />
            <div className=' border-t w-full mt-4' />
            <div className={cn('h-[calc(100vh-248px)] overflow-auto mt-2')}>
              {combineConcessionLiving?.length ? (
                <ul className='w-full flex  flex-col overflow-auto divide-y rounded'>
                  {combineConcessionLiving?.map((item) => (
                    <li
                      key={item.id}
                      aria-label={item.name_en}
                      className='w-full flex items-center group p-2 cursor-pointer gap-2 hover:bg-secondary'
                      onClick={() => {
                        console.log(item);
                      }}
                    >
                      <Image
                        width={56}
                        height={56}
                        priority={false}
                        className='object-contain'
                        src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${item.icon_path}`}
                        alt={item.name_en}
                      />
                      <article className=' leading-4'>
                        <p className=' line-clamp-1'>{item.name_en}</p>
                        <span className='text-xs text-muted-foreground'>
                          ({item.size})
                        </span>
                      </article>
                    </li>
                  ))}
                </ul>
              ) : (
                <p className='text-center text-sm text-muted-foreground mt-4'>
                  Empty
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <Drawer open={open} onOpenChange={setOpen}>
      <DrawerTrigger asChild>
        <div className='absolute top-8 right-4'>
          <TrigerButton />
        </div>
      </DrawerTrigger>
      <DrawerContent>
        <DrawerHeader className='text-left'>
          <DrawerTitle>My Concession and Living</DrawerTitle>
          <div className={cn('w-full md:w-80 rounded p-px mt-2')}>
            <Input placeholder='Search' className='z-20 bg-card' />
            <ScrollArea>
              <div className={cn('h-[calc(100vh-360px)] overflow-auto mt-3')}>
                {combineConcessionLiving?.length ? (
                  <ul className='w-full flex  flex-col overflow-auto divide-y rounded'>
                    {combineConcessionLiving?.map((item) => (
                      <li
                        key={item.id}
                        aria-label={item.name_en}
                        className='w-full flex items-center group p-2 cursor-pointer gap-2 hover:bg-secondary'
                        onClick={() => {
                          console.log(item);
                        }}
                      >
                        <Image
                          width={56}
                          height={56}
                          priority={false}
                          className='object-contain'
                          src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${item.icon_path}`}
                          alt={item.name_en}
                        />
                        <article className=' leading-4'>
                          <p className=' line-clamp-1'>{item.name_en}</p>
                          <span className='text-xs text-muted-foreground'>
                            ({item.size})
                          </span>
                        </article>
                      </li>
                    ))}
                  </ul>
                ) : (
                  <p className='text-center text-sm text-muted-foreground mt-4'>
                    Empty
                  </p>
                )}
              </div>
            </ScrollArea>
          </div>
        </DrawerHeader>
      </DrawerContent>
    </Drawer>
  );
};

export default MapSearchButton;
