'use client';
import Image from 'next/image';
import { cn, removeStorageGoogleapis } from '@/lib/utils';
import { Address } from 'viem';
import SkeletonWrapper from './skeleton-wrapper';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { Nft } from '@/types';
import { useEffect, useRef, useState } from 'react';
import Link from 'next/link';
import { CaretDownIcon } from '@radix-ui/react-icons';
import NFTItem from './nft-item';
type Props = {
  address: Address;
  name: string;
  image: string;
  nfts?: Array<Nft>;
  balance: number;
  isLoading: boolean;
};

const NFTProjectItem = ({
  address,
  name,
  image,
  nfts,
  balance,
  isLoading,
}: Props) => {
  const elementRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();

  const isCollapsed = searchParams.get('collapsed') === name;

  useEffect(() => {
    const currentElement = elementRef.current;

    const observer = new IntersectionObserver(
      ([entry]) => {
        // Update visibility state based on whether the element is in the viewport
        setIsVisible(entry.isIntersecting);
      },
      {
        threshold: 0.1, // Adjust as needed
      }
    );

    if (currentElement) {
      observer.observe(currentElement);
    }

    return () => {
      if (currentElement) {
        observer.unobserve(currentElement);
      }
    };
  }, [elementRef]);

  if (nfts?.length) {
    return (
      <div ref={elementRef} className='min-h-[76px]'>
        <SkeletonWrapper isLoading={isLoading}>
          {isVisible ? (
            <div
              className='flex items-center justify-between gap-2 w-full p-4 rounded-t bg-secondary/30 cursor-pointer'
              onClick={() => {
                const params = new URLSearchParams(searchParams);
                isCollapsed
                  ? params.delete('collapsed')
                  : params.set('collapsed', name);
                router.push(`${pathname}?${params}`, { scroll: false });
              }}
            >
              <div className='flex items-center gap-2'>
                <Image
                  className='rounded'
                  src={removeStorageGoogleapis(image)}
                  width={42}
                  height={42}
                  alt={name}
                />
                <article className='flex items-baseline space-x-2 text-lg'>
                  <p className='font-medium '>{name}</p>

                  {nfts?.length ? (
                    <p className='text-muted-foreground text-md'>
                      ({nfts?.length})
                    </p>
                  ) : (
                    <SkeletonWrapper isLoading={isLoading}>
                      <span className='m-0'>{`${balance || 0}`}</span>
                    </SkeletonWrapper>
                  )}
                </article>
              </div>
              {nfts?.length ? (
                <CaretDownIcon
                  className={cn(isCollapsed && 'rotate-180 transition-all')}
                />
              ) : null}
            </div>
          ) : null}
        </SkeletonWrapper>
        <div
          className={cn(
            'space-y-2 p-4 bg-secondary/20 hidden pt-0',
            isCollapsed && !isLoading && 'block'
          )}
        >
          {nfts?.map((nft) => (
            <NFTItem
              key={nft.address}
              address={nft.address as Address}
              name={nft.name}
              image={removeStorageGoogleapis(nft.image)}
            />
          ))}
        </div>
      </div>
    );
  }

  const parmas = new URLSearchParams(searchParams);

  return (
    <Link
      ref={elementRef}
      href={`${pathname}/${address}?${parmas}`}
      className='h-[76px] overflow-hidden'
    >
      <SkeletonWrapper isLoading={isLoading}>
        {isVisible ? (
          <div className='flex items-center justify-between gap-2 p-4 rounded bg-secondary/30'>
            <div className='flex items-center gap-2'>
              <Image
                className='rounded size-[42px]'
                src={removeStorageGoogleapis(image)}
                width={42}
                height={42}
                alt={name}
              />
              <article className='flex items-baseline space-x-2 text-lg'>
                <p className='font-medium '>{name}</p>

                {nfts?.length ? (
                  <p className='text-muted-foreground text-md'>
                    ({nfts?.length})
                  </p>
                ) : null}
              </article>
            </div>
            {nfts?.length ? (
              <CaretDownIcon />
            ) : (
              <SkeletonWrapper isLoading={isLoading}>
                <p className='m-0 font-bold'>{`${balance || 0}`}</p>
              </SkeletonWrapper>
            )}
          </div>
        ) : null}
      </SkeletonWrapper>
    </Link>
  );
};

export default NFTProjectItem;
