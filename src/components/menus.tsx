'use client';
import { cn } from '@/lib/utils';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

type Props = {
  className?: string;
  layout?: 'horizontal' | 'vertical';
  onClick?: () => void;
};

const MENUS = [
  {
    key: '/map',
    title: 'Map',
  },
  {
    key: '/my-concession',
    title: 'My Concession/Building',
  },
];

const Menus = ({ className, layout = 'horizontal', onClick }: Props) => {
  const pathname = usePathname();
  return (
    <ul
      className={cn(
        'flex gap-1',
        className,
        layout === 'horizontal' ? 'flex-col' : 'flex-row'
      )}
    >
      {MENUS.map((menu) => (
        <li
          key={menu.key}
          className={cn(
            'hover:text-primary p-2 px-3 rounded-md font-medium',
            pathname === menu.key && 'text-primary underline'
          )}
        >
          <Link
            onClick={() => {
              if (onClick) onClick();
            }}
            href={menu.key}
            className='flex w-full md:w-fit'
          >
            {menu.title}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Menus;
