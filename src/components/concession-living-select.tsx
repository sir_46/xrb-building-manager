import React from 'react';
import SkeletonWrapper from './skeleton-wrapper';
import { useQuery } from '@tanstack/react-query';
import buildingApi from '@/services/buildingApi';
import { Address } from 'viem';
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from './ui/select';
import Image from 'next/image';
import { useSearchParams } from 'next/navigation';
import { useSession } from 'next-auth/react';

export type CategoryType = 'Living' | 'Concession';

type Props = {
  value?: string;
  onValueChange: (value: any) => void;
};

const ConcessionLivingSelect = ({ value, onValueChange }: Props) => {
  const { data: session } = useSession();
  const searchParams = useSearchParams();
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const { data, isLoading } = useQuery({
    queryKey: ['my-buildings', session?.user?.wallet_address, city],
    queryFn: () =>
      buildingApi.fetchMyBuildings({
        city: city === 'bkcity' ? 'bkcity' : 'duncity',
        walletAddress: session?.user?.wallet_address as Address,
      }),
    enabled: !!session?.user?.wallet_address,
  });

  return (
    <SkeletonWrapper isLoading={isLoading}>
      <Select
        value={value}
        onValueChange={(value) => {
          const finded =
            data?.living?.find((liv) => liv?.building_id === value) ||
            data?.concession?.find((conc) => conc?.building_id === value);

          const living = data?.living?.find(
            (liv) => liv?.building_id === value
          );
          const concession = data?.concession?.find(
            (con) => con?.building_id === value
          );
          // const category = data?.living.
          if (living || concession) {
            const category = living ? 'Living' : 'Concession';

            console.log({ category, ...finded });
            onValueChange({ category, ...finded });
          }
        }}
      >
        <SelectTrigger className='w-48 bg-black'>
          <SelectValue placeholder='Living and Concession' />
        </SelectTrigger>
        <SelectContent>
          <SelectGroup>
            <SelectLabel>Living</SelectLabel>
            {data?.living.length ? (
              data?.living.map((item) => (
                <SelectItem
                  key={item.building_id}
                  value={item.building_id}
                  disabled={!item.enable}
                >
                  <div className='flex space-x-1'>
                    <Image
                      width={20}
                      height={20}
                      src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${item.icon_path}`}
                      alt={item.name_en}
                    />
                    <p className='w-32 text-start truncate'>{item.name_en}</p>
                  </div>
                </SelectItem>
              ))
            ) : (
              <SelectLabel className='text-xs font-light text-muted-foreground'>
                Empty
              </SelectLabel>
            )}
            <SelectLabel>Concession</SelectLabel>
            {data?.concession.length ? (
              data?.concession.map((item) => (
                <SelectItem
                  key={item.building_id}
                  value={item.building_id}
                  disabled={!item.enable}
                >
                  <div className='flex space-x-1'>
                    <Image
                      width={20}
                      height={20}
                      src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${item.icon_path}`}
                      alt={item.name_en}
                    />
                    <p className='w-32 text-start truncate'>{item.name_en}</p>
                  </div>
                </SelectItem>
              ))
            ) : (
              <SelectLabel className='text-xs font-light text-muted-foreground'>
                Empty
              </SelectLabel>
            )}
          </SelectGroup>
        </SelectContent>
      </Select>
    </SkeletonWrapper>
  );
};

export default ConcessionLivingSelect;
