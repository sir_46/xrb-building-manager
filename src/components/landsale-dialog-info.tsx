import landsaleApi from '@/services/landsaleApi';
import { useQuery } from '@tanstack/react-query';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { Dialog, DialogContent, DialogHeader, DialogTitle } from './ui/dialog';
import Image from 'next/image';
import { Badge } from './ui/badge';
import { imageSizes } from '@/lib/utils';

type Props = {
  variant: 'bkcity' | 'dungeon';
};

const LandsaleDialogInfo = ({ variant }: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const landsaleId = searchParams.get('landsale-id');
  const isOpen = searchParams.has('landsale-id');

  const { data: BKLandsale } = useQuery({
    queryKey: ['landsale', variant],
    queryFn: () => landsaleApi.fetchLandSales({ variant: variant }),
    select(data) {
      return data.find((land) => land.id === landsaleId);
    },
  });

  const onClose = useCallback(() => {
    const params = new URLSearchParams(searchParams);
    params.delete('landsale-id');

    router.push(`${pathname}?${params}`);
  }, [searchParams, pathname, router]);

  if (!BKLandsale) {
    return null;
  }

  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>{BKLandsale?.id}</DialogTitle>
        </DialogHeader>
        <div className='flex flex-col gap-4 py-4'>
          <div className='aspect-video relative'>
            <Image
              className='object-contain'
              src={`/landimages/${BKLandsale.size.toLowerCase()}.png`}
              fill
              sizes={imageSizes}
              alt={String(BKLandsale.area)}
            />
          </div>
          <article className='flex space-x-2'>
            <p>{BKLandsale.area}</p>
            <Badge variant='default'>{BKLandsale.size}</Badge>
          </article>
          <div>
            <p>X: {BKLandsale.x}</p>
            <p>Y: {BKLandsale.y}</p>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default LandsaleDialogInfo;
