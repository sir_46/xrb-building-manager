'use client';
import { Address } from 'viem';
import Image from 'next/image';
import { usePathname } from 'next/navigation';
import Link from 'next/link';
import { useQuery } from '@tanstack/react-query';
import { BKTestnet } from '@/lib/BKTestnet';
import { ticketAbi } from '@/lib/abi/tikcetAbi';
import SkeletonWrapper from './skeleton-wrapper';
import { useSession } from 'next-auth/react';

type Props = {
  address: Address;
  name: string;
  image: string;
};

const NFTItem = ({ address, name, image }: Props) => {
  const pathname = usePathname();
  const { data: session } = useSession();

  const { data: balanceOf, isLoading } = useQuery({
    queryKey: ['blanceOf', address],
    queryFn: () =>
      BKTestnet.readContract({
        address: address,
        abi: ticketAbi,
        functionName: 'balanceOf',
        args: [session?.user?.wallet_address],
      }),
    enabled: !!session,
  });

  return (
    <Link
      href={`${pathname}/${address}`}
      className='flex justify-between items-center'
    >
      <article className='flex space-x-2'>
        <Image
          width={32}
          height={32}
          className='rounded'
          src={image}
          alt={name}
        />
        <p>{name}</p>
      </article>
      <SkeletonWrapper isLoading={isLoading}>
        <p className='font-bold'>{balanceOf?.toString()}</p>
      </SkeletonWrapper>
    </Link>
  );
};

export default NFTItem;
