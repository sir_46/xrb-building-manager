import { Label } from './ui/label';
import { RadioGroup, RadioGroupItem } from './ui/radio-group';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from './ui/select';

type Props = {
  value: string;
  onChange?: (value: string) => void;
};

const options = [
  {
    key: 'bitkub-city-mutelulu',
    label: 'Bitkub City + Mutelulu',
  },
  {
    key: 'dungeon',
    label: 'Dungeon',
  },
];

const AreaRadio = ({ value, onChange }: Props) => {
  return (
    <RadioGroup value={value} onValueChange={(value) => onChange!(value)}>
      {options.map((option) => (
        <div className='flex items-center space-x-2' key={option.key}>
          <RadioGroupItem value={option.key} id={option.key} />
          <Label htmlFor={option.key}>{option.label}</Label>
        </div>
      ))}
    </RadioGroup>
  );
};

export default AreaRadio;
