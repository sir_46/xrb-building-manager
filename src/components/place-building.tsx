import L, { LatLngBoundsExpression, LatLngExpression } from 'leaflet';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  ImageOverlay,
  Marker,
  Popup,
  Rectangle,
  useMap,
  useMapEvents,
} from 'react-leaflet';
import {
  convertLatLonToXY,
  MAX_ZOOM,
  rectangleToGeoJSONPolygon,
} from './bk-city';
import Image from 'next/image';
import { capitalizeFirstLetter, getTopviewPath, imageSizes } from '@/lib/utils';
import { toast } from 'sonner';
import { IBuilding, IConcession, ILiving } from '@/types';
import { Button } from './ui/button';
import { CheckIcon, SymbolIcon } from '@radix-ui/react-icons';
import buildingApi, {
  CreateBuildingProps,
  UpdateBuildingProps,
} from '@/services/buildingApi';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { Loader } from 'lucide-react';
import { Badge } from './ui/badge';
import { useSearchParams } from 'next/navigation';
import { useSession } from 'next-auth/react';

type Props = {
  builging: ILiving | IConcession;
  myArea: Array<any>;
  onCancel?: () => void;
  // variant: 'bkcity' | 'dungeon';
  transformX?: number; // dungeon only
  transformY?: number; // dungeon only
};

function cmToDegrees(cm: number): number {
  const cmPerDegree = 11100000; // 11,100,000 cm per degree
  return cm / cmPerDegree;
}

type Rotation = 0 | 90 | 180 | 270;

const PlaceBuilding = ({
  builging,
  // variant = 'bkcity',
  transformX = 0,
  transformY = 0,
  onCancel,
}: Props) => {
  const queryCliene = useQueryClient();
  const [rotate, setRotate] = useState<Rotation>(0);
  const [bounds, setBounds] = useState<LatLngBoundsExpression | null>(null);
  const searchParams = useSearchParams();
  const map = useMap();

  const { data: session } = useSession();

  const imageOverlayRef = useRef<L.ImageOverlay | null>(null);
  const markerRef = useRef(null);

  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  useMapEvents({
    zoomend(e) {
      handleCalculateBounds();
    },
    move(e) {
      handleCalculateBounds();
    },
  });

  const getBounds = useCallback(
    (
      living: ILiving | IConcession,
      center: LatLngExpression
    ): LatLngBoundsExpression => {
      const position = convertLatLonToXY(
        L.latLng(center).lat || 0,
        L.latLng(center).lng || 0
      );

      const feature = rectangleToGeoJSONPolygon(
        position.x, // X center of map
        position.y, // Y center of map
        [90, 270].includes(rotate) ? living.w : living.d,
        [90, 270].includes(rotate) ? living.d : living.w,
        living.id
      );

      const bounds = [
        feature.geometry.coordinates[0][0],
        feature.geometry.coordinates[0][2],
      ];

      return bounds;
    },
    [rotate]
  );

  const { data: mybuilding } = useQuery({
    queryKey: ['my-building', session?.user?.wallet_address, city],
    queryFn: buildingApi.fetchPlacedBuildings,
    select(data) {
      return data.filter(
        (building: IBuilding) =>
          building.city === (city === 'bkcity' ? 'BKCITY' : 'DUNCITY')
      );
    },
    enabled: !!session?.user?.wallet_address,
  });

  const isUpdate = useCallback(() => {
    return mybuilding?.find(
      (building) => building.token_id.substring(0, 9) === builging.building_id
    )?.token_id
      ? true
      : false;
  }, [builging, mybuilding]);

  // Initialize bounds
  useEffect(() => {
    const center = map.getCenter();
    map.flyTo(center, MAX_ZOOM);

    setBounds(getBounds(builging, center));
  }, [builging, rotate, getBounds, map]);

  const placeBuilding = useMutation({
    mutationKey: ['place-building'],
    mutationFn: (paylaod: CreateBuildingProps) => buildingApi.createBuilding(paylaod),
    onSuccess(data) {
      toast.success('Your building has been placed.', {
        id: 'place-building',
        description: `${getPosition().x} ${getPosition().y}`,
      });

      queryCliene.refetchQueries({
        queryKey: ['my-building', session?.user?.wallet_address, city],
      });

      // Clear
      onCancel!();
    },
    onError(error: AxiosError) {
      const message = (error.response?.data as any).message || error.message;
      toast.error(message, {
        id: 'place-building',
      });
    },
  });

  const updateBuilding = useMutation({
    mutationKey: ['update-place-building'],
    mutationFn: (paylaod: UpdateBuildingProps) => buildingApi.updateBuilding(paylaod),
    onSuccess(data) {
      toast.success('Your building has been updated.', {
        id: 'updated-building',
        description: `${getPosition().x} ${getPosition().y}`,
      });

      queryCliene.refetchQueries({
        queryKey: ['my-building', session?.user?.wallet_address, city],
      });

      // Clear
      onCancel!();
    },
    onError(error: AxiosError) {
      const message = (error.response?.data as any).message || error.message;
      toast.error(message, {
        id: 'updated-building',
      });
    },
  });

  const handleCalculateBounds = useCallback(() => {
    const center = map.getCenter();
    const bounds: LatLngBoundsExpression = [
      [
        center.lat -
          cmToDegrees(
            ([90, 270].includes(rotate) ? builging.w : builging.d) * 100
          ) /
            2,
        center.lng -
          cmToDegrees(
            ([90, 270].includes(rotate) ? builging.d : builging.w) * 100
          ) /
            2,
      ],
      [
        center.lat +
          cmToDegrees(
            ([90, 270].includes(rotate) ? builging.w : builging.d) * 100
          ) /
            2,
        center.lng +
          cmToDegrees(
            ([90, 270].includes(rotate) ? builging.d : builging.w) * 100
          ) /
            2,
      ],
    ];

    setBounds(bounds);
  }, [map, builging, rotate]);

  const RenderImageOverLay = useCallback(() => {
    if (!bounds) {
      return null;
    }

    const marker = markerRef.current;

    if (marker) {
      (marker as any).openPopup();
    }

    const imageURL = `${process.env.NEXT_PUBLIC_IMAGE_HOST}${
      builging[getTopviewPath(rotate)]
    }`;

    return (
      <ImageOverlay
        ref={imageOverlayRef}
        key={builging.id}
        interactive
        alt={builging.name_en}
        url={imageURL}
        bounds={bounds}
      />
    );
  }, [builging, bounds, markerRef, rotate]);

  const getPosition = useCallback(() => {
    const center = map.getCenter();
    const { x, y } = convertLatLonToXY(center.lat, center.lng);
    return {
      x: Number(x.toFixed(0)) - transformX,
      y: Number(y.toFixed(0)) + transformY,
    };
  }, [map, transformX, transformY]);

  const onRotate = useCallback(() => {
    // Array of allowed rotation values
    const rotations: Rotation[] = [0, 90, 180, 270];
    const currentIndex = rotations.indexOf(rotate);
    const nextIndex = (currentIndex + 1) % rotations.length;
    setRotate(rotations[nextIndex]);
  }, [rotate]);

  return (
    <>
      <Marker
        ref={markerRef}
        position={map.getCenter()}
        interactive
        eventHandlers={{
          popupclose: (e) => {
            onCancel!();
          },
        }}
      >
        <Popup>
          <div className='w-32 space-y-4'>
            <p>{builging.name_en}</p>
            <div className=' aspect-video relative'>
              <Badge
                variant='outline'
                className=' text-[8px] text-border absolute top-px -right-2'
              >
                {builging.size}
              </Badge>
              <Image
                key={builging.name_en}
                src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${builging.icon_path}`}
                fill
                sizes={imageSizes}
                className='object-contain'
                alt={builging.name_en}
              />
            </div>
            <div className=' space-y-2'>
              <article className='grid grid-cols-3 justify-between'>
                <p className='m-0 text-center'>W: {builging.w}</p>
                <p className='m-0 text-center'>D: {builging.d}</p>
                <p className=' m-0 text-center'>R: {rotate}</p>
              </article>
              <article className='flex justify-between mb-0'>
                <p className='m-0'>X: {getPosition().x}</p>
                <p className=' m-0'>Y: {getPosition().y}</p>
              </article>
            </div>
            <div className='space-x-2 flex justify-between'>
              <Button
                size='sm'
                className='size-7 p-1 border'
                onClick={onRotate}
              >
                <SymbolIcon />
              </Button>
              <Button
                size='sm'
                variant='secondary'
                disabled={placeBuilding.isPending}
                className='size-7 p-1 bg-green-500 hover:bg-green-600'
                onClick={() => {
                  if (!session) {
                    return;
                  }
                  const payload: CreateBuildingProps = {
                    wallet_address: session?.user?.wallet_address,
                    area_id: [],
                    building_id: builging.building_id,
                    pos_x: Number(getPosition().x),
                    pos_y: Number(getPosition().y),
                    pos_z: 0, //init 0
                    rot_z: rotate,
                    city: city === 'bkcity' ? 'BKCITY' : 'DUNCITY',
                    category: capitalizeFirstLetter(builging.category) as
                      | 'Concession'
                      | 'Living',
                  };

                  if (isUpdate()) {
                    const tokenId = mybuilding?.find(
                      (item) =>
                        item.token_id.substring(0, 9) === builging.building_id
                    )?.token_id;

                    updateBuilding.mutate({
                      id: builging.id,
                      token_id: String(tokenId),
                      ...payload,
                    });
                    return;
                  }

                  placeBuilding.mutate(payload);
                }}
              >
                {placeBuilding.isPaused ? (
                  <Loader className='animate-spin size-4' />
                ) : (
                  <CheckIcon />
                )}
              </Button>
            </div>
          </div>
        </Popup>
      </Marker>
      {bounds ? <Rectangle key='building-rectangle' bounds={bounds} /> : null}
      <RenderImageOverLay />
    </>
  );
};

export default PlaceBuilding;
