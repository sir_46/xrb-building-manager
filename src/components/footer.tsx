import {
  DiscordLogoIcon,
  GitHubLogoIcon,
  TwitterLogoIcon,
} from '@radix-ui/react-icons';
import Logo from './logo';
import { FacebookIcon } from 'lucide-react';

type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className='mt-28'>
      <div className='container p-6'>
        <div className='grid grid-cols-1 grid-rows-1 sm:grid-cols-3 sm:grid-rows-2 md:grid-cols-5 md:grid-rows-1 gap-4'>
          <div>
            <Logo className='h-7' />
            <p className='text-sm text-muted-foreground mt-2'>
              Design Your Own Building Space in Your Preferred Style
            </p>
            <div className='flex gap-2 mt-2'>
              <TwitterLogoIcon className='size-6' />
              <DiscordLogoIcon className='size-6' />
              <GitHubLogoIcon className='size-6' />
            </div>
          </div>
          <div className=' sm:row-start-1 sm:col-start-3 md:col-start-2'>
            <h2>Product</h2>
            <p className='text-muted-foreground'>Bitkub Metaverce</p>
          </div>
          <div className='sm:col-start-1 md:col-start-3'>
            <h2>Resources</h2>
            <p className='text-muted-foreground'>About</p>
            <p className='text-muted-foreground'>Event</p>
            <p className='text-muted-foreground'>Tutorial</p>
          </div>
          <div className='sm:col-start-2 md:col-start-4'>
            <h2>Company</h2>
            <p className='text-muted-foreground'>Media</p>
            <p className='text-muted-foreground'>Blog</p>
          </div>
          <div className=' sm:col-start-3 md:col-start-5'>
            <h2>Legal</h2>
            <p className='text-muted-foreground'>Terms</p>
            <p className='text-muted-foreground'>Privacy</p>
            <p className='text-muted-foreground'>Support</p>
          </div>
        </div>
        <p className='text-center border-t py-6 mt-4'>
          © 2024 Building Manager. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
