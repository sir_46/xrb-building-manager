'use client';
import { Checkbox } from './ui/checkbox';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';

type Props = {};

const ShowNFTCheckbox = (props: Props) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  return (
    <div className='flex items-center space-x-2 my-4'>
      <Checkbox
        id='show-nft'
        checked={searchParams.has('show-nft')}
        onCheckedChange={(e) => {
          const params = new URLSearchParams(searchParams);

          if (e) {
            params.set('show-nft', e.toString());
          } else {
            params.delete('show-nft');
          }

          router.push(`${pathname}?${params}`);
        }}
        className='data-[state=checked]:bg-green-500 data-[state=checked]:border-green-500 data-[state=checked]:text-white'
      />
      <label
        htmlFor='show-nft'
        className='text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70'
      >
        Show My NFTs Only
      </label>
    </div>
  );
};

export default ShowNFTCheckbox;
