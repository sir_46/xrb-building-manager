import landsaleApi from '@/services/landsaleApi';
import { useQuery } from '@tanstack/react-query';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { rectangleToGeoJSONPolygon } from './bk-city';
import { GeoJSON } from 'react-leaflet';
import { GeoJsonObject } from 'geojson';
import LandsaleDialogInfo from './landsale-dialog-info';
import areaApi from '@/services/areaApi';
import { Address } from 'viem';
import { useSession } from 'next-auth/react';

type Props = {};

const BKCityLandsale = (props: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const { data: session } = useSession();

  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const { data: BKLandsale, isLoading } = useQuery({
    queryKey: ['bkcity-landsale'],
    queryFn: () => landsaleApi.fetchLandSales({ variant: 'bkcity' }),
  });

  const { data: myarea } = useQuery({
    queryKey: ['myarea', session?.user?.wallet_address, city],
    queryFn: () =>
      areaApi.getMyArea({
        walletAddress: session?.user?.wallet_address as Address,
      }),
    select(data) {
      return data.bkcity;
    },
    enabled: !!session?.user?.wallet_address,
  });

  const RenderLandsale = useCallback(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];

    const myareaIds = myarea?.map((area) => area.id);

    const features = BKLandsale?.filter(
      (land) => !myareaIds?.includes(land.id) // filter by my area
    )
      .filter((land) => sizes?.includes(land.size.toLowerCase()))
      .map((land) => {
        return rectangleToGeoJSONPolygon(
          land.x,
          land.y,
          land.w,
          land.d,
          land.id
        );
      });

    return features ? (
      <GeoJSON
        key={`dungeon-landsale-${sizes}`}
        pathOptions={{
          fillColor: '#D7B2FF',
          fillOpacity: 1,
          color: '#9D44FF',
          weight: 1,
          opacity: 1,
        }}
        eventHandlers={{
          click: (e) => {
            const id = e.propagatedFrom.feature.geometry.id;
            const params = new URLSearchParams(searchParams);
            params.set('landsale-id', `${id}`);

            router.push(`${pathname}?${params}`);
          },
        }}
        data={
          {
            type: 'MultiPolygon',
            crs: {
              type: 'name',
              properties: {
                name: 'urn:ogc:def:crs:OGC:1.3:CRS84',
              },
            },
            features: features,
          } as GeoJsonObject
        }
      />
    ) : null;
  }, [BKLandsale, myarea, router, pathname, searchParams]);

  return (
    <>
      <LandsaleDialogInfo variant='bkcity' /> {RenderLandsale()}
    </>
  );
};

export default BKCityLandsale;
