import { RotateCw } from 'lucide-react';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Button } from './ui/button';
import { ResetIcon, SymbolIcon } from '@radix-ui/react-icons';

type Props = {
  width: number; //cm
  height: number; //cm
  gridSize: number; //cm
  imageSrc?: string;
  livingWidth: number; //cm
  livingHeight: number; //cm
};

function cmToPx(cm: number, ppi: number = 96): number {
  const inches = cm / 2.54;
  const pixels = inches * ppi;
  return pixels;
}

function pxToCm(px: number, ppi: number = 96): number {
  const inches = px / ppi;
  const cm = inches * 2.54;
  return cm;
}

const LivingCanvas = ({
  width,
  height,
  gridSize,
  imageSrc,
  livingWidth,
  livingHeight,
}: Props) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [context, setContext] = useState<CanvasRenderingContext2D | null>(null);
  const [image, setImage] = useState<HTMLImageElement | null>(null);

  const [isDragging, setIsDragging] = useState(false);
  const [draggedImagePos, setDraggedImagePos] = useState<{
    x: number;
    y: number;
  }>({ x: 0, y: 0 });

  const [rotation, setRotation] = useState(0); // Rotation angle in degrees

  const drawImage = useCallback(
    (
      ctx: CanvasRenderingContext2D,
      img: HTMLImageElement,
      x: number,
      y: number,
      rotation: number
    ) => {
      const imgWidthPx = cmToPx(livingWidth);
      const imgHeightPx = cmToPx(livingHeight);

      // Clear the canvas before drawing the image (if needed)
      ctx.save();
      ctx.translate(x + imgWidthPx / 2, y + imgHeightPx / 2);
      ctx.rotate((rotation * Math.PI) / 180);

      // Draw the background with opacity
      ctx.globalAlpha = 0.3;
      ctx.fillStyle = 'white';
      ctx.fillRect(-imgWidthPx / 2, -imgHeightPx / 2, imgWidthPx, imgHeightPx);

      // Reset alpha for image and border drawing
      ctx.globalAlpha = 1.0;

      ctx.drawImage(
        img,
        -imgWidthPx / 2,
        -imgHeightPx / 2,
        imgWidthPx,
        imgHeightPx
      );

      ctx.restore();
      // console.log('Image drawn at:', x, y, 'with rotation:', rotation);
    },
    [livingWidth, livingHeight] // Dependencies for useCallback
  );

  useEffect(() => {
    const canvas = canvasRef.current;
    if (canvas) {
      const ctx = canvas.getContext('2d');
      setContext(ctx);
      if (ctx) {
        drawGrid(ctx, width, height, gridSize);

        if (imageSrc) {
          // Load the image
          const img = new Image();
          img.src = imageSrc;
          img.onload = () => {
            setImage(img);
            drawImage(ctx, img, 0, 0, rotation);
          };
        }
      }
    }
  }, [width, height, gridSize, imageSrc, rotation, drawImage]);

  useEffect(() => {
    const handleMouseMove = (event: MouseEvent) => {
      if (isDragging && context && image) {
        const rect = canvasRef.current?.getBoundingClientRect();
        const x = event.clientX - (rect?.left || 0);
        const y = event.clientY - (rect?.top || 0);

        // Snapped movement
        let snappedX = Math.round(x / cmToPx(gridSize)) * cmToPx(gridSize);
        let snappedY = Math.round(y / cmToPx(gridSize)) * cmToPx(gridSize);

        snappedX = Math.max(
          0,
          Math.min(cmToPx(width) - cmToPx(livingWidth), snappedX)
        );
        snappedY = Math.max(
          0,
          Math.min(cmToPx(height) - cmToPx(livingHeight), snappedY)
        );

        setDraggedImagePos({ x: snappedX, y: snappedY });

        context.clearRect(0, 0, width, height);
        drawGrid(context, width, height, gridSize);
        drawImage(context, image, snappedX, snappedY, rotation);
      }
    };

    const handleMouseUp = () => {
      setIsDragging(false);
    };

    window.addEventListener('mousemove', handleMouseMove);
    window.addEventListener('mouseup', handleMouseUp);

    return () => {
      window.removeEventListener('mousemove', handleMouseMove);
      window.removeEventListener('mouseup', handleMouseUp);
    };
  }, [
    isDragging,
    context,
    image,
    gridSize,
    width,
    height,
    livingWidth,
    livingHeight,
    rotation,
    drawImage,
  ]);

  const drawGrid = (
    ctx: CanvasRenderingContext2D,
    width: number,
    height: number,
    gridSize: number
  ) => {
    const majorGridSize = cmToPx(15);

    console.log('majorGridSize', majorGridSize);
    ctx.clearRect(0, 0, cmToPx(width), cmToPx(height));
    // ctx.strokeStyle = '#ddd';
    for (let x = 0; x <= cmToPx(width); x += cmToPx(gridSize)) {
      ctx.beginPath();
      if (majorGridSize === x) {
        ctx.strokeStyle = 'red'; // Color for major grid lines
      } else {
        ctx.strokeStyle = '#ddd'; // Color for regular grid lines
      }
      ctx.moveTo(x - 0.45, 0);
      ctx.lineTo(x - 0.45, cmToPx(height) - 1);
      ctx.stroke();
    }
    for (let y = 0; y <= cmToPx(height); y += cmToPx(gridSize)) {
      ctx.beginPath();
      if (majorGridSize === y) {
        ctx.strokeStyle = 'red'; // Color for major grid lines
      } else {
        ctx.strokeStyle = '#ddd'; // Color for regular grid lines
      }
      ctx.moveTo(0, y - 0.45);
      ctx.lineTo(cmToPx(width) - 1, y - 0.45);
      ctx.stroke();
    }
  };

  const handleMouseDown = (event: React.MouseEvent<HTMLCanvasElement>) => {
    if (context && image) {
      // const rect = canvasRef.current?.getBoundingClientRect();
      // const x = event.clientX - (rect?.left || 0);
      // const y = event.clientY - (rect?.top || 0);
      // const snappedX = Math.round(x / gridSize) * gridSize;
      // const snappedY = Math.round(y / gridSize) * gridSize;
      // setDraggedImagePos({ x: snappedX, y: snappedY });
      setIsDragging(true);
      console.log('Drag started');
    }
  };

  const handleReset = () => {
    if (context && image) {
      context.clearRect(0, 0, width, height);
      drawGrid(context, width, height, gridSize);
      drawImage(context, image, draggedImagePos.x, draggedImagePos.y, 0);
    }
  };

  const handleRotate = () => {
    setRotation((prevRotation) => {
      const newRotation = (prevRotation + 90) % 360;
      if (context && image) {
        context.clearRect(0, 0, width, height);
        drawGrid(context, width, height, gridSize);
        drawImage(
          context,
          image,
          draggedImagePos.x,
          draggedImagePos.y,
          newRotation
        );
      }
      return newRotation;
    });
  };
  return (
    <div>
      <div className=' absolute top-4 right-4'>
        <div className='flex space-x-2'>
          <p>Land:</p>
          <div className='flex flex-col'>
            <p>
              {width}x{height}cm
            </p>
            {/* <p>
              {cmToPx(width).toFixed(0)}x{cmToPx(height).toFixed(0)}px
            </p> */}
          </div>
        </div>
        <div className='flex space-x-2'>
          <p>Living:</p>
          <div className='flex flex-col'>
            <p>
              {livingWidth}x{livingWidth}cm
            </p>
            {/* <p>
              {cmToPx(livingWidth).toFixed(0)}x{cmToPx(livingWidth).toFixed(0)}
              px
            </p> */}
          </div>
        </div>
        <div>
          <p>X:{pxToCm(draggedImagePos.x).toFixed(0)}cm</p>
          <p>Y:{pxToCm(draggedImagePos.y).toFixed(0)}cm</p>
          <p>R: {rotation}</p>
        </div>
      </div>
      <div className=' text-center my-2'>
        <Button size='icon' variant='outline' onClick={handleReset}>
          <ResetIcon />
        </Button>
        <Button size='icon' variant='outline' onClick={handleRotate}>
          <SymbolIcon />
        </Button>
      </div>
      <canvas
        key='living-canvas'
        ref={canvasRef}
        width={cmToPx(width)}
        height={cmToPx(height)}
        className='bg-border'
        onMouseDown={handleMouseDown}
      />
    </div>
  );
};

export default LivingCanvas;
