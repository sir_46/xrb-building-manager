import React from 'react';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '../ui/form';
import { useForm } from 'react-hook-form';
import { MapFilterSchema, MapFilterSchemaType } from '@/shcema/map';
import AreaSelect from '../area-radio';
import SizeRadio from '../land-size-checkbox';
import { zodResolver } from '@hookform/resolvers/zod';
import LandTypeRadio from '../land-type-checkbox';
import { Button } from '../ui/button';

type Props = {
  onFinish: (values: MapFilterSchemaType) => void;
};

const MapFilterForm = ({ onFinish }: Props) => {
  // 1. Define your form.
  const form = useForm<MapFilterSchemaType>({
    resolver: zodResolver(MapFilterSchema),
    defaultValues: {
      area: 'bitkub-city',
      landType: 'public',
      size: 'xs',
    },
  });

  // 2. Define a submit handler.
  function onSubmit(values: MapFilterSchemaType) {
    onSubmit(values);
  }

  return (
    <Form {...form}>
      <form className='space-y-8' onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name='area'
          render={({ field }) => (
            <FormItem>
              <FormLabel>Area</FormLabel>
              <FormControl>
                <AreaSelect {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name='landType'
          render={({ field }) => (
            <FormItem>
              <FormLabel>Land Type</FormLabel>
              <FormControl>
                <LandTypeRadio {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name='size'
          render={({ field }) => (
            <FormItem>
              <FormLabel>Size</FormLabel>
              <FormControl>
                <SizeRadio {...field} />
              </FormControl>

              <FormMessage />
            </FormItem>
          )}
        />
        <div>
          <Button type='submit' className='w-full'>
            Submit
          </Button>
          <Button
            type='submit'
            variant='link'
            className='w-full mt-2'
            onClick={() => form.reset()}
          >
            Clear
          </Button>
        </div>
      </form>
    </Form>
  );
};

export default MapFilterForm;
