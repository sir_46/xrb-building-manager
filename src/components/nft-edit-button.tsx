import { useParams } from 'next/navigation';
import React from 'react';
import { Button, buttonVariants } from './ui/button';
import { Pencil } from 'lucide-react';
import { Nft } from '@/types';
import Link from 'next/link';
import { cn } from '@/lib/utils';

type Props = {
  data: Nft;
};

const NFTEditButton = ({ data }: Props) => {
  const params = useParams();
  const buildingId = params.id;

  return (
    <Link
      href={`/my-concession/${buildingId}/nfts/?tokenId=${data.token_id}&nftId=${data.id}`}
      className={cn(buttonVariants({ size: 'icon' }), 'rounded-full')}
    >
      <Pencil className=' size-4' />
    </Link>
  );
};

export default NFTEditButton;
