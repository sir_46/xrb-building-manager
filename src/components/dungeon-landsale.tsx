import landsaleApi from '@/services/landsaleApi';
import { useQuery } from '@tanstack/react-query';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { rectangleToGeoJSONPolygon } from './bk-city';
import { GeoJSON } from 'react-leaflet';
import { GeoJsonObject } from 'geojson';
import LandsaleDialogInfo from './landsale-dialog-info';

type Props = {
  transformX: number;
  transformY: number;
};

const DungeonLandsale = ({ transformX, transformY }: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();

  const { data: Landsale } = useQuery({
    queryKey: ['dungeon-landsale'],
    queryFn: () => landsaleApi.fetchLandSales({ variant: 'dungeon' }),
  });

  const RenderLandsale = useCallback(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];

    const features = Landsale?.filter((land) =>
      sizes?.includes(land.size.toLowerCase())
    ).map((land) => {
      return rectangleToGeoJSONPolygon(
        land.x + transformX,
        land.y - transformY,
        land.w,
        land.d,
        land.id
      );
    });

    return features ? (
      <GeoJSON
        key={`bk-landsale-${sizes}`}
        pathOptions={{
          fillColor: '#D7B2FF',
          fillOpacity: 1,
          color: '#9D44FF',
          weight: 1,
          opacity: 1,
        }}
        eventHandlers={{
          click: (e) => {
            const id = e.propagatedFrom.feature.geometry.id;
            const params = new URLSearchParams(searchParams);
            params.set('landsale-id', `${id}`);
            router.push(`${pathname}?${params}`);
          },
        }}
        data={
          {
            type: 'MultiPolygon',
            crs: {
              type: 'name',
              properties: {
                name: 'urn:ogc:def:crs:OGC:1.3:CRS84',
              },
            },
            features: features,
          } as GeoJsonObject
        }
      />
    ) : null;
  }, [Landsale, searchParams, pathname, router, transformX, transformY]);

  return (
    <>
      <LandsaleDialogInfo variant='dungeon' /> {RenderLandsale()}
    </>
  );
};

export default DungeonLandsale;
