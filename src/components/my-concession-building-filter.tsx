'use client';
import { cn } from '@/lib/utils';
import { ToggleGroup, ToggleGroupItem } from './ui/toggle-group';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useCallback } from 'react';

type Props = {
  className?: string;
};

const items = [{}];

const MyConcessionBuildingFilter = ({ className }: Props) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  const filter = searchParams.get('filter') || 'all';

  const handleChange = useCallback(
    (value: string) => {
      const params = new URLSearchParams(searchParams);
      params.set('filter', value);
      router.push(`${pathname}?${params}`);
    },
    [searchParams, pathname, router]
  );

  return (
    <div className={cn(className, 'w-fit')}>
      <ToggleGroup value={filter} type='single' onValueChange={handleChange}>
        <ToggleGroupItem value='all' aria-label='Toggle all'>
          All
        </ToggleGroupItem>
        <ToggleGroupItem value='concession' aria-label='Toggle concession'>
          Concession
        </ToggleGroupItem>
        <ToggleGroupItem value='building' aria-label='Toggle building'>
          Building
        </ToggleGroupItem>
      </ToggleGroup>
    </div>
  );
};

export default MyConcessionBuildingFilter;
