'use client';
import { useRouter, useSearchParams } from 'next/navigation';
import { useState } from 'react';
import Image from 'next/image';
import { signIn, useSession } from 'next-auth/react';
import { toast } from 'sonner';

type Props = {};

const CallbackClient = (props: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();

  const { data } = useSession();
  const [isLoading, setLoading] = useState(false);

  const code = searchParams.get('code');

  return (
    <div className='h-svh flex items-center justify-center flex-col gap-4'>
      <h1 className='text-center text-lg'>
        เลือกแอปพลิเคชั่นที่คุณต้องการใช้งาน
      </h1>
      <section>
        <button
          disabled={isLoading}
          className='flex items-center justify-center flex-col border p-2 rounded bg-secondary/30'
          onClick={async (e) => {
            toast.loading('Loading...', { id: 'bknext-login' });
            setLoading(true);
            try {
              const res = await signIn('credentials', {
                redirect: false,
                code,
              });

              if (res?.error) {
                toast.error('Login Error', {
                  id: 'bknext-login',
                  description: JSON.stringify(res.error, null, 2),
                });
                console.log(res);
                return false;
              }
            } catch (error) {
              console.log(error);
            }

            toast.success('Login Successful', {
              id: 'bknext-login',
              description: 'Redirecting you to the map page. Please wait.',
            });

            setLoading(false);
            router.push('/');
          }}
        >
          <Image
            width={112}
            height={112}
            alt='Bitkub NEXT'
            src='/bitkub-next-wallet.png'
            className=' rounded-sm overflow-hidden'
          />

          {
            <p className='text-sm mt-2'>
              {isLoading ? 'Loading...' : 'Bitkub NEXT'}
            </p>
          }
        </button>
      </section>

      <div
        className='flex h-8 items-center space-x-1'
        aria-live='polite'
        aria-atomic='true'
      >
        {data?.user && (
          <>
            <p className='text-sm text-green-500'>Welcome</p>
          </>
        )}
      </div>
    </div>
  );
};

export default CallbackClient;
