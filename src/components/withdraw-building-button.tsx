import React, { useCallback } from 'react';
import { Button } from './ui/button';
import { MapPinOff } from 'lucide-react';
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from './ui/alert-dialog';
import { toast } from 'sonner';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import buildingApi from '@/services/buildingApi';
import { usePathname, useSearchParams } from 'next/navigation';
import { IBuilding } from '@/types';
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';

type Props = {
  building: IBuilding;
};

const WithdrawBuildingButton = ({ building }: Props) => {
  const queryClient = useQueryClient();
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();

  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const { data: session } = useSession();
  const handleWithdraw = useCallback(() => {
    setTimeout(() => {
      toast.success('Door settings have been successfully withdraw.', {
        id: 'widthdraw-building',
      });
    }, 3000);
  }, []);

  const mutatuion = useMutation({
    mutationKey: ['widthdraw-building'],
    mutationFn: buildingApi.deleteBuilding,
    onMutate(variables) {
      toast.loading('Withdrawing...', {
        id: 'widthdraw-building',
      });
    },
    onSuccess(data, variables, context) {
      toast.success('The building has been successfully withdrawn.', {
        id: 'widthdraw-building',
      });

      const params = new URLSearchParams(searchParams);
      params.delete('my-building-id');
      router.push(`${pathname}?${params}`);

      queryClient.refetchQueries({
        queryKey: ['my-building', session?.user?.wallet_address, city],
      });

      queryClient.refetchQueries({
        queryKey: ['my-buildings', session?.user?.wallet_address, city],
      });
    },
    onError(error, variables, context) {
      toast.error(`${error.message}`, {
        id: 'widthdraw-building',
      });
    },
  });

  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button variant='destructive' type='submit' className='pl-3'>
          <MapPinOff className=' size-5 mr-2' /> Withdraw
        </Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
          <AlertDialogDescription>
            This action cannot be undone. This will permanently delete your
            account and remove your data from our servers.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction
            disabled={mutatuion.isPending}
            onClick={() => {
              mutatuion.mutate({ token_id: building.token_id });
            }}
          >
            Continue
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default WithdrawBuildingButton;
