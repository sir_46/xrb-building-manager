import { IconProps } from '@radix-ui/react-icons/dist/types';
import React from 'react';

export const Star = ({ ...props }: IconProps) => (
  <svg
    {...props}
    width='83'
    height='83'
    viewBox='0 0 83 83'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
  >
    <path
      d='M30.8058 0.933908L44.2744 37.3495L82.5456 30.8059L46.13 44.2745L52.6736 82.5456L39.205 46.13L0.93389 52.6736L37.3495 39.205L30.8058 0.933908Z'
      fill='url(#paint0_linear_15_1871)'
    />
    <defs>
      <linearGradient
        id='paint0_linear_15_1871'
        x1='19.276'
        y1='26.6521'
        x2='70.9091'
        y2='86.332'
        gradientUnits='userSpaceOnUse'
      >
        <stop stopColor='#2E1EE0' />
        <stop offset='1' stopColor='#C51EE0' />
      </linearGradient>
    </defs>
  </svg>
);

const Wallet = ({ ...props }: IconProps) => (
  <svg
    {...props}
    width='22'
    height='19'
    viewBox='0 0 22 19'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
  >
    <path
      d='M5 5.5H9'
      stroke='white'
      strokeWidth='1.5'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
    <path
      d='M19.833 6.5H17.231C15.446 6.5 14 7.843 14 9.5C14 11.157 15.447 12.5 17.23 12.5H19.833C19.917 12.5 19.958 12.5 19.993 12.498C20.533 12.465 20.963 12.066 20.998 11.565C21 11.533 21 11.494 21 11.417V7.583C21 7.506 21 7.467 20.998 7.435C20.962 6.934 20.533 6.535 19.993 6.502C19.959 6.5 19.917 6.5 19.833 6.5Z'
      stroke='white'
      strokeWidth='1.5'
    />
    <path
      d='M19.965 6.5C19.887 4.628 19.637 3.48 18.828 2.672C17.657 1.5 15.771 1.5 12 1.5H9C5.229 1.5 3.343 1.5 2.172 2.672C1 3.843 1 5.729 1 9.5C1 13.271 1 15.157 2.172 16.328C3.343 17.5 5.229 17.5 9 17.5H12C15.771 17.5 17.657 17.5 18.828 16.328C19.637 15.52 19.888 14.372 19.965 12.5'
      stroke='white'
      strokeWidth='1.5'
    />
    <path
      d='M16.991 9.5H17.001'
      stroke='white'
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </svg>
);

export const Icons = {
  Star,
  Wallet,
};
