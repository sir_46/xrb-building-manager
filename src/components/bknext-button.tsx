import { buttonVariants } from './ui/button';
import { cn } from '@/lib/utils';

import Link from 'next/link';

type Props = {};

const BKNextButton = ({}: Props) => {
  const client_id = process.env.BK_CLIENT_ID;
  const redirect_uri = process.env.NEXT_PUBLIC_BK_REDIRECT_URL;
  const query_Parameters = `response_type=code&client_id=${client_id}&redirect_uri=${redirect_uri}`;
  const url = `${process.env.NEXT_PUBLIC_BKNEXT_URL}/oauth2/authorize?${query_Parameters}`;

  return (
    <Link
      href={url}
      className={cn(
        buttonVariants(),
        'bg-gradient-to-r from-[#8D1CFE] to-[#0038ED] text-white w-52'
      )}
    >
      Login with Bitkub NEXT
    </Link>
  );
};

export default BKNextButton;
