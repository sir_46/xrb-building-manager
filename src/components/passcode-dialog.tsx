import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from './ui/dialog';
import { Button } from './ui/button';
import { InputOTP, InputOTPGroup, InputOTPSlot } from './ui/input-otp';

type Props = {
  open: boolean;
  onChange: (value: string) => void;
  onCancel: VoidFunction;
};

const MAX_LENGTH = 4;

const PasscodeDialog = ({ open, onChange, onCancel }: Props) => {
  const [value, setValue] = useState<string>('');
  return (
    <Dialog open={open} onOpenChange={onCancel}>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>Passcode</DialogTitle>
        </DialogHeader>
        <div className='flex justify-center'>
          <InputOTP
            maxLength={MAX_LENGTH}
            onChange={(value) => setValue(value)}
          >
            <InputOTPGroup>
              <InputOTPSlot index={0} />
              <InputOTPSlot index={1} />
              <InputOTPSlot index={2} />
              <InputOTPSlot index={3} />
            </InputOTPGroup>
          </InputOTP>
        </div>
        <DialogFooter>
          <Button
            type='submit'
            disabled={value.length != MAX_LENGTH}
            onClick={() => onChange!(value)}
          >
            Ok
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default PasscodeDialog;
