'use client';
import { NFTToken } from '@/types';
import {
  cn,
  extractAddresses,
  fetchBalance,
  mergeArrayOfRecords,
} from '@/lib/utils';
import { useQuery } from '@tanstack/react-query';
import NFTProjectItem from './nft-project-item';
import SkeletonWrapper from './skeleton-wrapper';
import { useCallback, useEffect, useState } from 'react';
import { Address } from 'viem';
import { useSearchParams } from 'next/navigation';
import { useSession } from 'next-auth/react';

type Props = {
  className?: string;
};

const NFTTokenList = ({ className }: Props) => {
  const { data: session } = useSession();

  const searchParams = useSearchParams();

  const { data: tokens, isLoading } = useQuery({
    queryKey: ['nft-tokens'],
    queryFn: () =>
      fetch('https://next-api.bitkubnft.com/api/nft-tokens')
        .then((res) => res.json())
        .then((res) => res.data as Array<NFTToken>),
  });

  const {
    data: balanceOfAll,
    isLoading: balanceLoading,
    isFetched,
  } = useQuery({
    queryKey: ['balanceOfAll', session?.user.wallet_address],
    queryFn: async () => {
      const addresses = extractAddresses(tokens || []);

      try {
        const balancess = await Promise.all(
          addresses.map((address) =>
            fetchBalance(String(address), String(session?.user?.wallet_address))
              .then((res) => {
                return { [address as any]: +`${res?.toString()}` || 0 };
              })
              .catch((err) => {
                return { [address as any]: 0 };
              })
          )
        );

        return mergeArrayOfRecords(balancess) as Record<string, number>;
      } catch (error) {
        console.error('Error fetching balances:', error);
      }
    },
    enabled: !!session && !!tokens,
  });

  const filteredMyNFTOnly = useCallback(() => {
    const showMyNFTOnly = searchParams.has('show-nft');

    if (showMyNFTOnly && balanceOfAll) {
      return tokens?.filter((token) => {
        const balanceOf =
          token.type === 'group'
            ? token.nfts
                ?.map((nft) => Number(`${balanceOfAll![nft.address]}` || 0))
                .reduce((acc, curr) => acc + curr, 0) || 0
            : Number(`${balanceOfAll![token.address]}` || 0);

        return balanceOf > 0;
      });
    }

    return tokens;
  }, [tokens, balanceOfAll, searchParams]);

  return (
    <div className={cn(className)}>
      <SkeletonWrapper isLoading={isLoading}>
        <div className='flex flex-col gap-2 min-h-52'>
          {filteredMyNFTOnly()?.map((token, idx) => {
            const balance = balanceOfAll?.[token.address] || 0;

            return (
              <NFTProjectItem
                isLoading={balanceLoading || !isFetched}
                balance={balance}
                key={token.name}
                address={token.address}
                name={token.name}
                nfts={token.nfts}
                image={token.image}
              />
            );
          })}
        </div>
      </SkeletonWrapper>
    </div>
  );
};

export default NFTTokenList;
