import React from 'react';
import { Form, FormControl, FormField, FormItem, FormLabel } from './ui/form';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { Checkbox } from './ui/checkbox';
import { LandSizeShema, LandSizeShemaType } from '@/shcema/map';
import { useSearchParams } from 'next/navigation';

type Props = {
  onChange?: (value: Array<string>) => void;
};

const items = [
  {
    id: 'xs',
    label: 'XS',
  },
  {
    id: 's',
    label: 'S',
  },
  {
    id: 'm',
    label: 'M',
  },
  {
    id: 'l',
    label: 'L',
  },
  {
    id: 'xl',
    label: 'XL',
  },
];

const LandSizeCheckbox = ({ onChange }: Props) => {
  const searchParams = useSearchParams();
  const defaultValues =
    searchParams.get('size')?.split(',') || items.map((item) => item.id);

  const form = useForm<LandSizeShemaType>({
    resolver: zodResolver(LandSizeShema),
    defaultValues: {
      items: defaultValues,
    },
  });

  const onSubmit = (values: any) => {
    console.log(values);
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        onChange={() => {
          if (onChange) {
            onChange!(form.getValues().items);
          }
        }}
        className='space-y-8'
      >
        <FormField
          control={form.control}
          name='items'
          render={() => (
            <FormItem>
              {items.map((item) => (
                <FormField
                  key={item.id}
                  control={form.control}
                  name='items'
                  render={({ field }) => {
                    return (
                      <FormItem
                        key={item.id}
                        className='flex flex-row items-start space-x-3 space-y-0'
                      >
                        <FormControl>
                          <Checkbox
                            checked={field.value?.includes(item.id)}
                            onCheckedChange={(checked) => {
                              return checked
                                ? field.onChange([...field.value, item.id])
                                : field.onChange(
                                    field.value?.filter(
                                      (value) => value !== item.id
                                    )
                                  );
                            }}
                          />
                        </FormControl>
                        <FormLabel className='text-sm font-normal'>
                          {item.label}
                        </FormLabel>
                      </FormItem>
                    );
                  }}
                />
              ))}
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
};

export default LandSizeCheckbox;
