'use client';
import { useQuery } from '@tanstack/react-query';
import { AspectRatio } from './ui/aspect-ratio';
import { cn } from '@/lib/utils';
import { usePathname, useSearchParams } from 'next/navigation';
import SkeletonWrapper from './skeleton-wrapper';
import Link from 'next/link';
import { Address } from 'viem';
import concessionApi, { ConcesssionFilterType } from '@/services/concessionApi';
import ImageFallback from './image-fallback';
import { useSession } from 'next-auth/react';

type Props = {
  className?: string;
};

const MyConcessionBuildingList = ({ className }: Props) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const filter = (searchParams.get('filter') || 'all') as ConcesssionFilterType;

  const { data: session } = useSession();

  const { data, isLoading, isFetched } = useQuery({
    queryKey: ['my-concessions', filter],
    queryFn: () =>
      concessionApi.fetchMyConcessions({
        walletAddress: session?.user?.wallet_address as Address,
        filter: filter,
      }),
    enabled: !!filter && !!session,
  });

  return (
    <div className={cn(className)}>
      <SkeletonWrapper isLoading={isLoading || !isFetched}>
        {data?.length ? (
          <ul className='grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-4  min-w-full min-h-48'>
            {data?.map((concession) => (
              <li
                key={concession.id}
                className={cn('bg-secondary/30 border rounded-md')}
              >
                <Link
                  className={cn(
                    concession.category !== 'building' && 'cursor-not-allowed'
                  )}
                  href={
                    concession.category === 'building'
                      ? `${pathname}/${concession.building_id}`
                      : '#'
                  }
                  scroll={false}
                >
                  <AspectRatio ratio={1 / 1}>
                    <ImageFallback
                      className='object-contain p-2 rounded'
                      alt={concession.name_en}
                      fill
                      src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${concession.icon_path}`}
                    />
                  </AspectRatio>
                  <div className='p-2'>
                    <p className='line-clamp-1'>{concession.name_en}</p>
                    <div className='border-t my-2' />
                    <div className='flex justify-between '>
                      <div className=' size-7 rounded-full border grid place-content-center text-xs'>
                        {concession.size}
                      </div>
                      <div className='text-sm text-muted-foreground'>
                        {concession.frame ? `${concession.frame}/Item` : null}
                      </div>
                    </div>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        ) : (
          <h2 className='text-center texxt-xl my-8 text-muted-foreground min-h-48'>
            No concessions and building available at the moment.
          </h2>
        )}
      </SkeletonWrapper>
    </div>
  );
};

export default MyConcessionBuildingList;
