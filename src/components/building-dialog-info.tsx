'use client';
import React, { useCallback } from 'react';
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from './ui/dialog';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import Image from 'next/image';
import { useQuery } from '@tanstack/react-query';
import buildingApi from '@/services/buildingApi';
import { Badge } from './ui/badge';
import { imageSizes } from '@/lib/utils';
import WithdrawBuildingButton from './withdraw-building-button';

type Props = {};

const BuildingDialogInfo = (props: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const buildingId =
    searchParams.get('building-id') || searchParams.get('my-building-id');
  const isOpen =
    searchParams.has('building-id') || searchParams.has('my-building-id');
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const onClose = useCallback(() => {
    const params = new URLSearchParams(searchParams);

    params.delete('building-id');
    params.delete('my-building-id');

    router.push(`${pathname}?${params}`);
  }, [searchParams, pathname, router]);

  const { data: building } = useQuery({
    queryKey: ['my-building', buildingId, city],
    queryFn: buildingApi.fetchPlacedBuildings,
    select(data) {
      return data.find((building) => building.token_id === buildingId);
    },
    enabled: !!buildingId,
  });

  if (!building) {
    return null;
  }

  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>{building.token_id}</DialogTitle>
        </DialogHeader>
        <div className='flex flex-col gap-4 py-4'>
          <div className='aspect-video relative'>
            <Image
              className='object-contain'
              src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${building.icon_path}`}
              fill
              sizes={imageSizes}
              alt={String(building?.name_en)}
            />
          </div>
          <article className='flex space-x-2'>
            <p>{building.name_en}</p>
            <Badge variant='default'>{building.size}</Badge>
          </article>
          <div>
            <p>X: {building.pos_x}</p>
            <p>Y: {building.pos_y}</p>
            <p>Z: {building.pos_z}</p>
            <p>R: {building.rot_z}</p>
          </div>
        </div>
        {searchParams.has('my-building-id') ? (
          <DialogFooter>
            {building ? <WithdrawBuildingButton building={building} /> : null}
          </DialogFooter>
        ) : null}
      </DialogContent>
    </Dialog>
  );
};

export default BuildingDialogInfo;
