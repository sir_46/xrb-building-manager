'use client';

import { useSearchParams } from 'next/navigation';
import React from 'react';
import Dungeon from './dungeon';
import BKCity from './bk-city';

type Props = {};

const MapSwicther = (props: Props) => {
  const searchParams = useSearchParams();
  const area = searchParams.get('area');

  if (area === 'dungeon') {
    return <Dungeon />;
  }

  return <BKCity />;
};

export default MapSwicther;
