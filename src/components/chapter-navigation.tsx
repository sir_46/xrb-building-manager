'use client';

import { ChevronLeft, ChevronRight } from 'lucide-react';
import { ReactNode, useCallback, useEffect, useRef, useState } from 'react';

type Props = {
  itemCount: number;
  children: ReactNode;
};

const ChapterNavigation = ({ itemCount, children }: Props) => {
  const chapternavRef = useRef<HTMLDivElement | null>(null);
  const [scrollLeft, setScollLeft] = useState<number>(0);

  // const scollRef = useRef<HTMLDivElement | null>(null);

  const [chapterW, setChapterW] = useState<number>(0);
  const [itemW, setItemW] = useState<number>(0);

  const updateWindowSize = useCallback(() => {
    const scoll = chapternavRef.current;
    const childElm = scoll?.firstElementChild;
    const itemW = document.getElementById('chapter-item');
    if (scoll && childElm && itemW) {
      setChapterW(scoll.clientWidth);
      setItemW(itemW?.clientWidth + 2);
    }
  }, [chapternavRef]);

  // Attach the scroll listener to the div
  useEffect(() => {
    window.addEventListener('resize', updateWindowSize);

    updateWindowSize();

    return () => window.removeEventListener('resize', updateWindowSize);
  }, [updateWindowSize]);

  const onPrev = useCallback(() => {
    const current = chapternavRef.current?.scrollLeft || 0;

    // scoll position start
    if (current === 0) return null;

    console.log('prev', current);
    chapternavRef.current?.scrollTo({
      top: 0,
      left: current <= chapterW / 2 ? 0 : Math.abs(current - chapterW / 2),
      behavior: 'smooth',
    });
  }, [chapterW, chapternavRef]);

  const onNext = useCallback(() => {
    const scrollW = itemW * itemCount;
    const current = chapternavRef.current?.scrollLeft || 0;

    // scoll position end
    if (current === scrollW) return null;

    chapternavRef.current?.scrollTo({
      top: 0,
      left: chapterW / 2 + current,
      behavior: 'smooth',
    });
  }, [itemW, itemCount, chapterW, chapternavRef]);

  return (
    <div className='relative'>
      {scrollLeft !== 0 ? (
        <div
          className='absolute h-full flex items-center top-0 -left-[26px] border-r border-zinc-600 cursor-pointer'
          onClick={onPrev}
        >
          <ChevronLeft />
        </div>
      ) : null}
      <div
        ref={chapternavRef}
        className='overflow-auto no-scrollbar relative'
        onScroll={(e) => {
          setScollLeft(e.currentTarget.scrollLeft);
        }}
      >
        {children}
      </div>
      {scrollLeft < itemW * itemCount - chapterW ? (
        <div
          className='absolute h-full flex items-center top-0 -right-[26px] border-l border-zinc-600 cursor-pointer'
          onClick={onNext}
        >
          <ChevronRight />
        </div>
      ) : null}
    </div>
  );
};

export default ChapterNavigation;
