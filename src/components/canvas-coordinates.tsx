'use client';
import React, { useRef, useEffect } from 'react';

interface CanvasProps {
  width: number;
  height: number;
  tileSize: number;
}

const CanvasWithCoordinates: React.FC<CanvasProps> = ({
  width,
  height,
  tileSize,
}) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const context = canvas.getContext('2d');
    if (!context) return;

    const width = canvas.width;
    const height = canvas.height;

    // Clear canvas
    context.clearRect(0, 0, width, height);

    // Draw X and Y axis
    context.beginPath();
    context.moveTo(0, height / 2);
    context.lineTo(width, height / 2);
    context.moveTo(width / 2, 0);
    context.lineTo(width / 2, height);
    context.strokeStyle = '#fff';
    context.stroke();

    // Draw ticks on X axis
    for (let i = -10; i <= 10; i++) {
      const x = (i + 10) * (width / 20);
      context.beginPath();
      context.moveTo(x, height / 2 - 5);
      context.lineTo(x, height / 2 + 5);
      context.strokeStyle = 'red';
      context.stroke();
      context.fillStyle = 'red';
      context.fillText(i.toString(), x - 3, height / 2 + 15);
    }

    // Draw ticks on Y axis
    for (let i = -10; i <= 10; i++) {
      const y = (i + 10) * (height / 20);
      context.beginPath();
      context.moveTo(width / 2 - 5, y);
      context.lineTo(width / 2 + 5, y);
      context.strokeStyle = 'red';
      context.stroke();
      context.fillStyle = 'red';
      context.font = '20px';
      context.fillText((-i).toString(), width / 2 + 10, y + 3);
    }
  }, []);

  return <canvas ref={canvasRef} width={1024} height={1024} />;
};

export default CanvasWithCoordinates;
