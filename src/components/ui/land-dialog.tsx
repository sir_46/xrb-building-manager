'use client';
import * as React from 'react';
import * as DialogPrimitive from '@radix-ui/react-dialog';
import { Cross2Icon } from '@radix-ui/react-icons';

import { cn } from '@/lib/utils';

const LandDialog = DialogPrimitive.Root;

const LandDialogPortal = DialogPrimitive.Portal;

const LandDialogClose = DialogPrimitive.Close;

const LandDialogOverlay = React.forwardRef<
  React.ElementRef<typeof DialogPrimitive.Overlay>,
  React.ComponentPropsWithoutRef<typeof DialogPrimitive.Overlay>
>(({ className, ...props }, ref) => (
  <DialogPrimitive.Overlay
    ref={ref}
    className={cn(
      'fixed inset-0 z-50 data-[state=open]:animate-in data-[state=closed]:animate-out data-[state=closed]:fade-out-0 data-[state=open]:fade-in-0',
      className
    )}
    {...props}
  />
));

LandDialogOverlay.displayName = DialogPrimitive.Overlay.displayName;

const LandDialogContent = React.forwardRef<
  React.ElementRef<typeof DialogPrimitive.Content>,
  React.ComponentPropsWithoutRef<typeof DialogPrimitive.Content>
>(({ className, children, ...props }, ref) => (
  <LandDialogPortal>
    <LandDialogOverlay />
    <DialogPrimitive.Content
      ref={ref}
      className={cn(
        'fixed right-0 md:right-4 top-0 md:top-[88px] z-50 grid w-full max-w-none md:max-w-lg gap-4 border bg-background p-6 shadow-lg duration-200 data-[state=open]:animate-in data-[state=closed]:animate-out data-[state=closed]:fade-out-0 data-[state=open]:fade-in-0 data-[state=closed]:zoom-out-95 data-[state=open]:zoom-in-95 data-[state=closed]:slide-out-to-bottom-1/2 md:data-[state=closed]:slide-out-to-bottom-1 md:data-[state=closed]:slide-out-to-right-1/2 data-[state=open]:slide-in-from-bottom-1/2 md:data-[state=open]:slide-in-from-bottom-1 md:data-[state=open]:slide-in-from-right-1/2 rounded-none md:rounded',
        className
      )}
      {...props}
    >
      {children}
      <DialogPrimitive.Close className='absolute right-4 top-4 rounded-sm opacity-70 ring-offset-background transition-opacity hover:opacity-100 focus:outline-none focus:ring-2 focus:ring-ring focus:ring-offset-2 disabled:pointer-events-none data-[state=open]:bg-accent data-[state=open]:text-muted-foreground'>
        <Cross2Icon className='h-4 w-4' />
        <span className='sr-only'>Close</span>
      </DialogPrimitive.Close>
    </DialogPrimitive.Content>
  </LandDialogPortal>
));

LandDialogContent.displayName = DialogPrimitive.Content.displayName;

export {
  LandDialog,
  LandDialogPortal,
  LandDialogOverlay,
  LandDialogClose,
  LandDialogContent,
};
