'use client';
import { cn, fallbackImage, imageSizes } from '@/lib/utils';
import Image from 'next/image';
import { useEffect, useState } from 'react';

type Props = {
  alt: string;
  src: string;
  className?: string;
  fill: boolean;
};

const ImageFallback = ({
  className,
  fill = false,
  alt,
  src,
  ...props
}: Props) => {
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    setError(false);
  }, [src]);

  return (
    <Image
      className={cn(className)}
      {...props}
      alt={alt}
      fill={fill}
      sizes={fill ? imageSizes : ''}
      onError={() => setError(true)}
      src={error ? fallbackImage : src}
    />
  );
};

export default ImageFallback;
