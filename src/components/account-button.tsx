'use client';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from './ui/dropdown-menu';
import { cn, truncatedAddress } from '@/lib/utils';
import { Button } from './ui/button';
import { IUser } from '@/types';
import { Icons } from './icons';
import { deleteCookie } from 'cookies-next';
import { CopyIcon } from '@radix-ui/react-icons';
import { useCopyToClipboard } from 'usehooks-ts';
import { toast } from 'sonner';
import { UserRound } from 'lucide-react';
import { signOut } from 'next-auth/react';
import SkeletonWrapper from './skeleton-wrapper';

type Props = {
  isLoading: boolean;
  user: IUser;
};

const AccountButton = ({ user, isLoading }: Props) => {
  const [_copiedText, copy] = useCopyToClipboard();

  return (
    <SkeletonWrapper isLoading={isLoading}>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button className='bg-gradient-to-r from-[#8D1CFE] to-[#0038ED] text-white'>
            <Icons.Wallet className='mr-2' />
            {truncatedAddress(String(user?.wallet_address))}
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56 mr-6'>
          <DropdownMenuLabel>
            <div className='flex items-center gap-2 w-fit'>
              <div className=' size-9 bg-secondary rounded-full grid place-content-center'>
                <UserRound className=' size-4' />
              </div>
              <div className='flex flex-col w-fit'>
                <p>{user?.display_name || user?.username}</p>
                <article className='text-sm text-muted-foreground font-normal flex items-center gap-1'>
                  <span className=' truncate w-[130px]'>
                    {user?.wallet_address}
                  </span>
                  <Button
                    variant='ghost'
                    className={cn('size-6 p-1')}
                    onClick={() => {
                      copy(user?.wallet_address).then(() =>
                        toast.success(`Copied `, {
                          description: user?.wallet_address,
                        })
                      );
                    }}
                  >
                    <CopyIcon />
                  </Button>
                </article>
              </div>
            </div>
          </DropdownMenuLabel>
          <DropdownMenuSeparator />
          <DropdownMenuItem
            onClick={() => {
              localStorage.clear();

              // Delete cookies
              deleteCookie('access_token');
              deleteCookie('refresh_token');
              deleteCookie('expires_in');

              signOut();
            }}
          >
            Log out
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </SkeletonWrapper>
  );
};

export default AccountButton;
