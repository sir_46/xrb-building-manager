import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { Dialog, DialogContent, DialogHeader, DialogTitle } from './ui/dialog';
import Image from 'next/image';
import areaApi from '@/services/areaApi';
import { useQuery } from '@tanstack/react-query';
import { Badge } from './ui/badge';
import { Address } from 'viem';

import { imageSizes } from '@/lib/utils';
import { useSession } from 'next-auth/react';

type Props = {};

const AreaDialogInfo = (props: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const areaId = searchParams.get('area-id');

  const { data } = useSession();
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const isOpen = searchParams.has('area-id');

  const onClose = useCallback(() => {
    const params = new URLSearchParams(searchParams);
    params.delete('area-id');

    router.push(`${pathname}?${params}`);
  }, [searchParams, pathname, router]);

  const { data: myarea } = useQuery({
    queryKey: ['myarea', areaId, city],
    queryFn: () =>
      areaApi.getMyArea({
        walletAddress: data?.user?.wallet_address as Address,
      }),
    select(data) {
      return data[city === 'bkcity' ? 'bkcity' : 'dungeon']?.find(
        (area) => area.id === areaId
      );
    },
    enabled: !!areaId && !!data,
  });

  if (!myarea) {
    return null;
  }
  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>{myarea?.id}</DialogTitle>
        </DialogHeader>
        <div className='flex flex-col gap-4 py-4'>
          <div className='aspect-video relative'>
            <Image
              className='object-contain'
              src={`/landimages/${myarea.size.toLowerCase()}.png`}
              fill
              sizes={imageSizes}
              alt={String(myarea.area)}
            />
          </div>
          <article className='flex space-x-2'>
            <p>{myarea.area}</p>
            <Badge variant='default'>{myarea.size}</Badge>
          </article>
          <div>
            <p>X: {myarea.x}</p>
            <p>Y: {myarea.y}</p>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default AreaDialogInfo;
