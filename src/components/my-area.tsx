import areaApi from '@/services/areaApi';
import { useQuery } from '@tanstack/react-query';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useCallback } from 'react';
import { rectangleToGeoJSONPolygon } from './bk-city';
import { GeoJsonObject } from 'geojson';
import { GeoJSON } from 'react-leaflet';
import AreaDialogInfo from './area-dialog-info';
import { Address } from 'viem';
import { useSession } from 'next-auth/react';

type Props = {
  transformX?: number;
  transformY?: number;
};

const MyArea = ({ transformX = 0, transformY = 0 }: Props) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();
  const { data: session } = useSession();
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  const { data: myarea } = useQuery({
    queryKey: ['myarea', session?.user?.wallet_address, city],
    queryFn: () =>
      areaApi.getMyArea({
        walletAddress: session?.user?.wallet_address as Address,
      }),
    enabled: !!session?.user?.wallet_address,
    select(data) {
      return city === 'bkcity' ? data.bkcity : data.dungeon;
    },
  });

  const RenderMyArea = useCallback(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];

    const features = myarea
      ?.filter((land) => sizes?.includes(land.size.toLowerCase()))
      .map((item) =>
        rectangleToGeoJSONPolygon(
          item.x + transformX,
          item.y - transformY,
          item.w,
          item.d,
          item.id
        )
      );

    return features ? (
      <GeoJSON
        key={`myarea-${sizes}`}
        pathOptions={{
          fillColor: '#9D44FF',
          fillOpacity: 1,
          color: '#9D44FF',
          weight: 1,
          opacity: 1,
        }}
        eventHandlers={{
          click: (e) => {
            const id = e.propagatedFrom.feature.geometry.id;
            const params = new URLSearchParams(searchParams);
            params.set('area-id', `${id}`);
            router.push(`${pathname}?${params}`);
          },
        }}
        data={
          {
            type: 'FeatureCollection',
            crs: {
              type: 'name',
              properties: {
                name: 'urn:ogc:def:crs:OGC:1.3:CRS84',
              },
            },
            features: features,
          } as GeoJsonObject
        }
      />
    ) : null;
  }, [myarea, router, pathname, searchParams, transformX, transformY]);

  return (
    <>
      <AreaDialogInfo /> {RenderMyArea()}
    </>
  );
};

export default MyArea;
