'use client';

import { ArrowLeftIcon } from '@radix-ui/react-icons';
import Image from 'next/image';
import { useMutation, useQuery } from '@tanstack/react-query';
import { IPFS, NFTToken } from '@/types';
import { useParams, useRouter, useSearchParams } from 'next/navigation';
import SkeletonWrapper from './skeleton-wrapper';
import {
  cn,
  imageSizes,
  removeStorageGoogleapis,
  truncatedNFTToken,
} from '@/lib/utils';
import { Button } from './ui/button';
import { BKMainnet } from '@/lib/BKMainnet';
import { ticketAbi } from '@/lib/abi/tikcetAbi';
import { Address } from 'viem';
import { useCallback, useState } from 'react';
import { Check } from 'lucide-react';
import { AspectRatio } from './ui/aspect-ratio';
import { Badge } from './ui/badge';
import { toast } from 'sonner';
import nftAPI, { NFTCreateType } from '@/services/nftApi';
import concessionApi from '@/services/concessionApi';
import { useSession } from 'next-auth/react';
type Props = {
  className?: string;
};

const NFTList = ({ className }: Props) => {
  const params = useParams();
  const router = useRouter();
  const searchParams = useSearchParams();
  const { data: session } = useSession();

  const buildingId = params.id;
  const tokenId = searchParams.get('tokenId');
  const ntfId = searchParams.get('nftId');

  const [selectTokenId, setSelectTokenId] = useState<string | null>(tokenId);

  const { data: usedNFTs } = useQuery({
    queryKey: ['my-concession', buildingId],
    queryFn: () =>
      concessionApi.fetchConcessionDetails({ buildingId: String(buildingId) }),
    enabled: !!buildingId,
    select(data) {
      return data.nfts;
    },
  });

  const { data: project, isLoading: projectLoading } = useQuery({
    queryKey: ['nft-project', params.address],
    queryFn: () =>
      fetch('https://next-api.bitkubnft.com/api/nft-tokens')
        .then((res) => res.json())
        .then((res) => res.data as Array<NFTToken>),
    enabled: !!params.address,
    select(data) {
      for (const nft of data) {
        if (nft.address === params.address) {
          return nft;
        }
        // Check inside nested arrays if applicable
        if (nft.type === 'group' && nft.nfts) {
          const nestedNft = nft.nfts.find(
            (innerNft) => innerNft.address === params.address
          );
          if (nestedNft) {
            return nestedNft;
          }
        }
      }
      return undefined;
    },
  });

  const { data: tokenOfOwnerAll, isLoading: tokenOfOwnerLoaing } = useQuery({
    queryKey: ['tokenOfOwnerAll', params.address],
    queryFn: async () => {
      const result = await BKMainnet.readContract({
        address: params.address as Address,
        abi: ticketAbi,
        functionName: 'tokenOfOwnerAll',
        args: [session?.user?.wallet_address],
      });

      return result as Array<bigint>;
    },
    enabled: !!project?.address && !!session?.user?.wallet_address,
  });

  const { data: tokenURIS, isLoading: tokenURISLoading } = useQuery({
    queryKey: ['tokenURI', tokenOfOwnerAll?.toString()],
    queryFn: async () => {
      const results = await Promise.all(
        (tokenOfOwnerAll || []).map((token) =>
          BKMainnet.readContract({
            address: String(params.address) as Address,
            abi: ticketAbi,
            functionName: 'tokenURI',
            args: [token],
          })
        )
      );

      return results as Array<string>;
    },
    enabled: !!tokenOfOwnerAll,
  });

  const { data: NFTs, isLoading: isNFTsLoading } = useQuery({
    queryKey: ['nfts', tokenURIS],
    queryFn: async () => {
      try {
        const results = await Promise.all(
          (tokenURIS || []).map((uri) =>
            fetch(`${uri}`).then((res) => res.json())
          )
        );
        return results as Array<IPFS>;
      } catch (error) {}
    },
    enabled: !!tokenURIS?.length,
  });

  const createNFTmutation = useMutation({
    mutationKey: ['create-nft', buildingId],
    mutationFn: nftAPI.createNFT,
    onMutate() {
      toast.loading(`'Adding your NFT...'`, {
        id: 'add-nft',
      });
    },
    onSuccess() {
      toast.success(`Your NFT has been added.`, {
        id: 'add-nft',
      });

      router.push(`/my-concession/${params.id}`);
    },
    onError(error) {
      toast.error(`${error.message}`, { id: 'add-nft' });
      console.log(error);
    },
  });

  const updateNFTmutation = useMutation({
    mutationKey: ['update-nft', buildingId],
    mutationFn: nftAPI.updateNFT,
    onMutate() {
      toast.loading(`'Updating your NFT...'`, {
        id: 'update-nft',
      });
    },
    onSuccess() {
      toast.success(`Your NFT has been updated.`, {
        id: 'update-nft',
      });

      router.push(`/my-concession/${params.id}`);
    },
    onError(error) {
      toast.error('Failed to update your NFT.', { id: 'update-nft' });
      console.log(error);
    },
  });

  const handleAddNFTtoConcession = useCallback(() => {
    if (!selectTokenId) {
      return;
    }
    const tokenIndex = tokenOfOwnerAll
      ?.map((token) => String(token))
      .indexOf(selectTokenId);

    const selectd = NFTs![tokenIndex || 0];

    const payload: NFTCreateType = {
      project_name: String(project?.name),
      nft_name: selectd.name,
      contract_address: params.address as Address,
      token_id: String(selectTokenId),
      building_id: String(buildingId),
      image: selectd.image,
    };

    ntfId
      ? updateNFTmutation.mutate({ ...payload, id: ntfId })
      : createNFTmutation.mutate(payload);
  }, [
    NFTs,
    selectTokenId,
    updateNFTmutation,
    createNFTmutation,
    buildingId,
    ntfId,
    tokenOfOwnerAll,
    project,
    params,
  ]);

  const rarity = useCallback(
    (index: number) => {
      const rarity = NFTs![index].attributes?.find(
        (attrs) => attrs.trait_type === 'Rarity'
      )?.value;
      return String(rarity || 'Normal').substring(0, 1);
    },
    [NFTs]
  );

  const usedNFT = useCallback(
    (token: string) => {
      // Iterate through the provided array of NFTs
      for (const nft of usedNFTs || []) {
        // Check if the current NFT's token_id matches the provided one
        if (nft.token_id === token) {
          // If there's a match, return true
          return true;
        }
      }

      // If no match is found, return false
      return false;
      // return usedNFTs?.find((nft) => nft.token_id === token) || false;
    },
    [usedNFTs]
  );

  return (
    <div className={cn(className)}>
      <div className='flex justify-between items-center gap-2'>
        <div className='flex items-center gap-2'>
          <Button size='icon' variant='ghost' onClick={() => router.back()}>
            <ArrowLeftIcon />
          </Button>
          <SkeletonWrapper isLoading={projectLoading}>
            <div className='flex items-center gap-2 min-w-52 min-h-9 line-clamp-1'>
              {project ? (
                <Image
                  className='object-contain rounded'
                  src={removeStorageGoogleapis(project.image)}
                  width={36}
                  height={36}
                  alt={project.name}
                />
              ) : null}
              {/* <SkeletonWrapper isLoading={isLoading}> */}
              <h1 className='font-black text-xl line-clamp-1'>
                {project?.name}
              </h1>
              {/* </SkeletonWrapper> */}
            </div>
          </SkeletonWrapper>
        </div>
        <Button
          disabled={selectTokenId === searchParams.get('tokenId')}
          onClick={handleAddNFTtoConcession}
        >
          {tokenId ? 'Update' : 'Confirm'}
        </Button>
      </div>
      <SkeletonWrapper
        isLoading={
          tokenOfOwnerLoaing ||
          tokenURISLoading ||
          projectLoading ||
          isNFTsLoading
        }
      >
        <div className='grid grid-cols-2 sm:grid-cols-3  md:grid-cols-4 lg:grid-cols-5 gap-4 mt-4 min-h-52'>
          {(NFTs || []).length > 0 ? (
            NFTs?.map((nft, idx) => (
              <div
                key={String(
                  tokenOfOwnerAll?.length ? tokenOfOwnerAll![idx] : ''
                )}
                className=' border rounded overflow-hidden bg-secondary/30 group'
              >
                {tokenOfOwnerAll ? (
                  <div className='relative aspect-square'>
                    {usedNFT(`${tokenOfOwnerAll![idx]}`) ? (
                      <div className='absolute top-2 right-2 z-10'>
                        <Badge>Used</Badge>
                      </div>
                    ) : (
                      <span
                        className={cn(
                          'grid place-content-center absolute bg-secondary top-2 right-2 size-6 rounded-full border border-white z-10 cursor-pointer',
                          selectTokenId === `${tokenOfOwnerAll![idx]}` &&
                            'bg-green-500 border-green-500'
                        )}
                        onClick={() => {
                          setSelectTokenId(`${tokenOfOwnerAll![idx]}`);
                        }}
                      >
                        {selectTokenId === `${tokenOfOwnerAll![idx]}` ? (
                          <Check className='size-3' />
                        ) : null}
                      </span>
                    )}

                    <AspectRatio ratio={1 / 1}>
                      <Image
                        className={cn(
                          'group-hover:grayscale-0 object-contain',
                          typeof selectTokenId === 'string' && 'grayscale',
                          selectTokenId === `${tokenOfOwnerAll![idx]}` &&
                            'grayscale-0'
                        )}
                        fill
                        sizes={imageSizes}
                        src={removeStorageGoogleapis(nft.image)}
                        alt={String(tokenOfOwnerAll![idx])}
                      />
                    </AspectRatio>

                    {tokenOfOwnerAll ? (
                      <Badge className='absolute bottom-2 right-2'>{`${truncatedNFTToken(
                        String(tokenOfOwnerAll![idx])
                      )}`}</Badge>
                    ) : null}
                  </div>
                ) : null}
                <article className='p-2'>
                  <p className='text-sm min-h-10 line-clamp-2'>{nft.name}</p>
                  <Badge variant='outline'>{rarity(idx)}</Badge>
                </article>
              </div>
            ))
          ) : (
            <article className='grid place-content-center col-start-1 col-end-6 text-center max-h-52'>
              <p className='text-muted-foreground'>
                Your NFT Collection is Empty
              </p>
            </article>
          )}
        </div>
      </SkeletonWrapper>
    </div>
  );
};

export default NFTList;
