import { useParams } from 'next/navigation';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import nftAPI from '@/services/nftApi';
import { toast } from 'sonner';
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from './ui/alert-dialog';
import { Button } from './ui/button';
import { Trash } from 'lucide-react';
type Props = {
  id: number;
};

const NFTDeleteButton = ({ id }: Props) => {
  const params = useParams();
  const buildingId = params.id;

  const queryClient = useQueryClient();

  const mutation = useMutation({
    mutationKey: ['delte-nft', buildingId],
    mutationFn: nftAPI.deleteNFT,
    onMutate() {
      toast.loading('Deleting your NFT...', {
        id: 'delete-nft',
      });
    },
    onSuccess() {
      toast.success('Your NFT has been successfully deleted.', {
        id: 'delete-nft',
      });
      queryClient.refetchQueries({ queryKey: ['my-concession', buildingId] });
    },
    onError(error) {
      toast.error('Failed to delete your NFT.', {
        description: error.message,
        id: 'delete-nft',
      });
    },
  });

  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button size='icon' variant='destructive' className='rounded-full'>
          <Trash className=' size-4' />
        </Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
          <AlertDialogDescription>
            This action cannot be undone. This will permanently delete your
            account and remove your data from our servers.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={() => mutation.mutate({ id: id })}>
            Continue
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default NFTDeleteButton;
