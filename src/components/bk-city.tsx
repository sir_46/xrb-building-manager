'use client';
import {
  LayersControl,
  MapContainer,
  TileLayer,
  ZoomControl,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css';
import { LatLngBoundsExpression, LatLngExpression } from 'leaflet';
import L from 'leaflet';

import { useSearchParams } from 'next/navigation';
import proj4 from 'proj4';
import { useQuery } from '@tanstack/react-query';
import areaApi from '@/services/areaApi';
import { Suspense, useCallback, useMemo, useState } from 'react';
import { IConcession, ILiving } from '@/types';
import PlaceBuilding from './place-building';
import MyBuildingOverlay from './my-building-overlay';
import MyArea from './my-area';
import BKCityLandsale from './bk-city-landsale';
import { Address } from 'viem';

import { Loader } from 'lucide-react';
import ConcessionLivingSelect from './concession-living-select';
import { useSession } from 'next-auth/react';

type Props = {};

export const MIN_ZOOM = 14;
export const MAX_ZOOM = 18;

const EPSG3857 = 'EPSG:3857';
const EPSG4326 = 'EPSG:4326';

// Function to rotate a point
export function rotatePoint(
  point: L.LatLng,
  angle: number,
  center: L.LatLng
): L.LatLng {
  const rad = (angle * Math.PI) / 180; // Convert angle to radians
  const sin = Math.sin(rad);
  const cos = Math.cos(rad);

  // Translate point to origin
  const translatedX = point.lat - center.lat;
  const translatedY = point.lng - center.lng;

  // Rotate point
  const rotatedX = translatedX * cos - translatedY * sin;
  const rotatedY = translatedX * sin + translatedY * cos;

  // Translate back
  const newLat = rotatedX + center.lat;
  const newLng = rotatedY + center.lng;

  return L.latLng(newLat, newLng);
}

export const convertXYToLatLng = (x: number, y: number): L.LatLng => {
  // Convert cm to meters
  const xMeters = x / 100;
  const yMeters = y / 100;

  const [lng, lat] = proj4(EPSG3857, EPSG4326, [xMeters, yMeters]);
  return L.latLng(lat, lng);
};

export const convertLatLonToXY = (
  lat: number,
  lon: number
): { x: number; y: number } => {
  // Transform the lat, lon coordinates to x, y in meters
  const [xMeters, yMeters] = proj4(EPSG4326, EPSG3857, [
    Number(lon),
    Number(lat),
  ]);

  // Convert meters to centimeters
  const x = xMeters * 100;
  const y = yMeters * 100;

  return { x, y: y * -1 };
};

export const rectangleToGeoJSONPolygon = (
  x: number,
  y: number,
  w: number,
  d: number,
  id: string | number
): any => {
  // Convert dimensions from cm to meters
  const dm = d * 100;
  const wm = w * 100;

  // Calculate the corners based on the center point (x, y)
  const halfW = wm / 2;
  const halfD = dm / 2;

  const points = [
    { x: x - halfW, y: y - halfD }, // Top-left
    { x: x + halfW, y: y - halfD }, // Top-right
    { x: x + halfW, y: y + halfD }, // Bottom-right
    { x: x - halfW, y: y + halfD }, // Bottom-left
  ];

  const coordinates = points.map((point) => {
    const latLng = convertXYToLatLng(point.x, point.y * -1);
    return [latLng.lng, latLng.lat];
  });

  if (coordinates.length > 0) {
    coordinates.push(coordinates[0]); // Close the polygon
  }

  return {
    type: 'Feature',
    geometry: {
      type: 'Polygon',
      id: id,
      coordinates: [coordinates],
    },
    properties: {},
  };
};

const BKCity = ({}: Props) => {
  const searchParams = useSearchParams();

  const { data: session } = useSession();

  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';

  // Coordinates for the center of the square
  const center: LatLngExpression = [0, 0];
  // Define the bounds of the rectangle (4.9km x 4.9km)
  const delta = 0.04981; // Approximate degree equivalent for 4.9km, adjust for more accuracy

  // Define the max bounds (4.9 + 1.5km)
  const maxBounds: LatLngBoundsExpression = [
    [center[0] - (delta + 0.015) / 2, center[1] - (delta + 0.015) / 2],
    [center[0] + (delta + 0.015) / 2, center[1] + (delta + 0.015) / 2],
  ];

  const types = searchParams.get('type')?.split(',') || [
    'living-house',
    'my-land',
    'land',
    'living-house',
  ];

  const [selectBuilding, setSelectBuilding] = useState<
    ILiving | IConcession | null
  >(null);

  const { data: myarea } = useQuery({
    queryKey: ['myarea', session?.user?.wallet_address, city],
    queryFn: () =>
      areaApi.getMyArea({
        walletAddress: session?.user?.wallet_address as Address,
      }),
    enabled: !!session?.user?.wallet_address,
  });

  const myAreaGeojson = useMemo(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];
    const features = myarea?.bkcity
      ?.filter((item) => sizes?.includes(item.size.toLowerCase()))
      .map((item) =>
        rectangleToGeoJSONPolygon(item.x, item.y, item.w, item.d, item.id)
      );
    return features || [];
  }, [myarea, searchParams]);

  const handleClearPlacement = useCallback(() => {
    setSelectBuilding(null);
  }, []);

  return (
    <>
      {!selectBuilding?.building_id ? (
        <div className='absolute top-6 z-[2000] flex space-x-1'>
          <ConcessionLivingSelect
            value={selectBuilding?.building_id}
            onValueChange={(value) => {
              setSelectBuilding(value);
            }}
          />
        </div>
      ) : null}
      <MapContainer
        id='bk-city'
        center={center}
        zoom={MIN_ZOOM + 2}
        scrollWheelZoom={true}
        className='w-screen h-[calc(100dvh-64px)] bg-transparent'
        zoomControl={false}
        maxBounds={maxBounds}
      >
        <ZoomControl position='bottomleft' />
        <TileLayer
          noWrap={true}
          maxZoom={MAX_ZOOM}
          minZoom={MIN_ZOOM}
          eventHandlers={{
            click: (e) => {
              console.log(e);
            },
          }}
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">Bitkub City</a> contributors'
          url='bkcity/{z}/{x}/{y}.png'
        />

        <Suspense fallback={<Loader className='animate-spin' />}>
          {selectBuilding ? (
            <PlaceBuilding
              myArea={myAreaGeojson}
              builging={selectBuilding}
              onCancel={() => {
                handleClearPlacement();
              }}
            />
          ) : null}
        </Suspense>
        <LayersControl>
          <LayersControl.BaseLayer
            checked={types.includes('land')}
            name='Landsale'
          >
            <BKCityLandsale />
          </LayersControl.BaseLayer>
          <LayersControl.Overlay
            checked={types.includes('my-land')}
            name='My area'
          >
            <MyArea />
          </LayersControl.Overlay>
          <LayersControl.Overlay
            checked={types.includes('living-house')}
            name='My building'
          >
            <MyBuildingOverlay />
          </LayersControl.Overlay>
        </LayersControl>
      </MapContainer>
    </>
  );
};

export default BKCity;
