'use client';

import { useCallback, useEffect, useRef, useState } from 'react';

interface Tile {
  x: number;
  y: number;
  data: ImageData;
}

interface CanvasProps {
  width: number;
  height: number;
  tileSize: number;
}

const MapCanvas = ({ width, height, tileSize }: CanvasProps) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [viewport, setViewport] = useState({
    width: Number(window?.innerWidth || 0),
    height: Number(window?.innerHeight - 64 || 0),
  });

  useEffect(() => {
    const handleResize = () => {
      setViewport({
        width: window?.innerWidth,
        height: window?.innerHeight - 64,
      });
    };

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const drawCoordinates = useCallback(
    (canvas: HTMLCanvasElement) => {
      if (!canvas) return;

      const ctx = canvas.getContext('2d');
      if (!ctx) return;

      // Clear the canvas
      // ctx.clearRect(0, 0, viewport.width, viewport.height);

      // Calculate scale factors
      const scaleX = viewport.width / width;
      const scaleY = viewport.height / height;
      const scale = Math.min(scaleX, scaleY); // Preserve aspect ratio

      // Calculate translation to center the canvas
      const translateX = viewport.width / 2;
      const translateY = viewport.height / 2;

      // Apply scaling and translation
      ctx.save(); // Save the current ctx state
      ctx.translate(256, 256); // Translate to center
      ctx.scale(scale, scale); // Apply the scale transformation

      // Draw X and Y axis
      ctx.beginPath();
      ctx.moveTo(0, height / 2);
      ctx.lineTo(width, height / 2);
      ctx.moveTo(width / 2, 0);
      ctx.lineTo(width / 2, height);
      ctx.strokeStyle = 'red';
      ctx.stroke();

      // Draw ticks on X axis
      console.log('-----------------');
      for (let i = -512; i <= 512; i += 256) {
        console.log(i);
        const x = i;
        ctx.beginPath();
        ctx.moveTo(x, -512);
        ctx.lineTo(x, -512);
        ctx.strokeStyle = 'red';
        ctx.stroke();
        ctx.fillStyle = 'red';
        ctx.font = '20px Arial';
        ctx.fillText(i.toString(), i, height / 2 + 15);
      }

      ctx.restore(); // Restore the context to its original state

      // // Draw ticks on Y axis
      // for (let i = -10; i <= 10; i++) {
      //   const y = (i + 10) * (height / 20);
      //   ctx.beginPath();
      //   ctx.moveTo(width / 2 - 5, y);
      //   ctx.lineTo(width / 2 + 5, y);
      //   ctx.strokeStyle = 'red';
      //   ctx.stroke();
      //   ctx.fillStyle = 'red';
      //   ctx.font = '20px';
      //   ctx.fillText((-i).toString(), width / 2 + 10, y + 3);
      // }
    },
    [viewport, width, height]
  );

  const drawTiles = useCallback(
    (canvas: HTMLCanvasElement) => {
      if (!canvas) return;

      const ctx = canvas.getContext('2d');
      if (!ctx) return;

      // Clear the canvas
      // ctx.clearRect(0, 0, viewport.width, viewport.height);

      // Set text properties for indices
      ctx.font = '20px Arial';
      ctx.fillStyle = 'black';
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';

      // Calculate scale factors
      const scaleX = viewport.width / width;
      const scaleY = viewport.height / height;
      const scale = Math.min(scaleX, scaleY); // Preserve aspect ratio

      // Calculate translation to center the canvas
      const translateX = (viewport.width - width * scale) / 2 - 0 * scale;
      const translateY = (viewport.height - height * scale) / 2 - 0 * scale;

      // Apply scaling and translation
      ctx.save(); // Save the current context state
      ctx.translate(translateX, translateY); // Translate to center
      ctx.scale(scale, scale); // Apply the scale transformation

      // // Render tiles
      let y = 0;
      let x = 0;
      for (y = 0; y < height; y += tileSize) {
        for (x = 0; x < width; x += tileSize) {
          // Draw background color
          // ctx.fillStyle = `rgb(${Math.floor((255 * x) / width)}, ${Math.floor(
          //   (255 * y) / height
          // )}, 128)`;
          // ctx.fillRect(x, y, tileSize, tileSize);

          // Calculate the index
          const numTilesX = Math.ceil(x / tileSize);
          const numTilesY = Math.ceil(y / tileSize);

          // Draw stroke
          ctx.strokeStyle = '#fff';
          ctx.strokeRect(x, y, tileSize, tileSize);

          // Draw index
          ctx.fillStyle = '#fff';
          ctx.fillText(
            `${numTilesX} - ${numTilesY}`,
            x + tileSize / 2,
            y + tileSize / 2
          );

          // Load or reuse the image
          const newImg = new Image();
          newImg.src = `/tiles/${numTilesX}/${numTilesY}.png`; // adjust path accordingly
          newImg.onload = function () {
            // Draw the image tile
            ctx.drawImage(newImg, x, y, tileSize, tileSize); // Or at whatever offset you like
          };
        }
      }
      ctx.restore(); // Restore the context to its original state
    },
    [viewport, width, height, tileSize]
  );

  const drawMaker = useCallback(
    (canvas: HTMLCanvasElement) => {
      if (!canvas) return;

      const ctx = canvas.getContext('2d');
      if (!ctx) return;

      // Clear the canvas
      // ctx.clearRect(0, 0, viewport.width, viewport.height);

      // Calculate scale factors
      const scaleX = viewport.width / width;
      const scaleY = viewport.height / height;
      const scale = Math.min(scaleX, scaleY); // Preserve aspect ratio

      // Calculate translation to center the canvas
      const translateX = viewport.width / 2;
      const translateY = viewport.height / 2;

      // Apply scaling and translation
      ctx.save(); // Save the current context state
      ctx.translate(translateX, translateY); // Translate to center
      ctx.scale(scale, scale); // Apply the scale transformation

      // Draw marker at position 0,0
      const markerSize = 20;
      ctx.fillStyle = 'red';
      ctx.beginPath();
      ctx.arc(0, 0, markerSize, 0, 2 * Math.PI);
      ctx.fill();
      ctx.fillStyle = '#fff';
      ctx.fillText('0,0', 0, 0);

      // Draw marker at position 0,512
      ctx.fillStyle = 'red';
      ctx.beginPath();
      ctx.arc(512, 0, markerSize, 0, 2 * Math.PI);
      ctx.fill();
      ctx.fillStyle = '#fff';
      ctx.fillText('512,0', 512, 0);

      // Draw marker at position 0,512
      ctx.fillStyle = 'red';
      ctx.beginPath();
      ctx.arc(-512, 0, markerSize, 0, 2 * Math.PI);
      ctx.fill();
      ctx.fillStyle = '#fff';
      ctx.fillText('-512,0', -512, 0);

      ctx.restore(); // Restore the context to its original state
    },
    [viewport, width, height]
  );

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas?.getContext('2d');

    if (ctx && canvas) {
      // drawCoordinates(canvas);
      drawTiles(canvas);
      // drawMaker(canvas);

      // ctx.restore(); // Restore the context to its original state
    }
  }, [drawTiles]);

  return (
    <canvas
      className='relative'
      ref={canvasRef}
      width={viewport.width}
      height={viewport.height}
      style={{ width: '100%', height: '100%' }}
    />
  );
};

export default MapCanvas;
