'use client';
import Logo from './logo';
import Menus from './menus';
import HamburgerButton from './hamburger-button';
import useScrollPosition from '@/hooks/useScrollPosition';
import { cn } from '@/lib/utils';
import AccountButton from './account-button';
import { signOut, useSession } from 'next-auth/react';

type Props = {};

const Nnavbar = (props: Props) => {
  const scrollPosition = useScrollPosition();
  const { data, status } = useSession();

  if (status === 'unauthenticated') {
    signOut();
  }
  const isLoading = status === 'loading';

  return (
    <nav
      className={cn(
        'fixed left-0 right-0 z-20 bg-transparent transition-colors',
        scrollPosition > 64 && 'backdrop-blur-md'
      )}
    >
      <div className='container flex justify-between items-center h-16'>
        <div className='flex items-center gap-4'>
          <HamburgerButton />
          <Logo className='h-8' />
          <Menus layout='vertical' className='hidden md:flex' />
        </div>
        {data ? (
          <AccountButton isLoading={isLoading} user={data?.user} />
        ) : null}
      </div>
    </nav>
  );
};

export default Nnavbar;
