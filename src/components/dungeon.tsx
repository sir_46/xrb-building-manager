'use client';
import {
  MapContainer,
  TileLayer,
  ZoomControl,
  LayersControl,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css';
import { LatLngBoundsExpression, LatLngExpression } from 'leaflet';
import { useQuery } from '@tanstack/react-query';
import { useSearchParams } from 'next/navigation';
import DungeonLandsale from './dungeon-landsale';
import MyBuildingOverlay from './my-building-overlay';
import { Suspense, useMemo, useState } from 'react';
import ConcessionLivingSelect from './concession-living-select';
import { Loader } from 'lucide-react';
import PlaceBuilding from './place-building';
import areaApi from '@/services/areaApi';
import { rectangleToGeoJSONPolygon } from './bk-city';
import { Address } from 'viem';
import { IConcession, ILiving } from '@/types';
import { useSession } from 'next-auth/react';
import MyArea from './my-area';

type Props = {};

// Transfrom center point
const TRANSFROM_X = 251400;
const TRANSFROM_Y = 349500;

const MIN_ZOOM = 15;
const MAX_ZOOM = 18;

const pathOptions = {
  fillColor: '#D7B2FF',
  fillOpacity: 1,
  color: '#9D44FF',
  weight: 1,
  opacity: 1,
};

const Dungeon = (props: Props) => {
  // Coordinates for the center of the square
  const center: LatLngExpression = [0, 0];
  // Define the bounds of the rectangle (3.5km x 3.5km)
  const delta = 0.03182; // Approximate degree equivalent for 3.5km, adjust for more accuracy
  const bounds: LatLngBoundsExpression = [
    [center[0] - delta / 2, center[1] - delta / 2],
    [center[0] + delta / 2, center[1] + delta / 2],
  ];

  const searchParams = useSearchParams();
  const city = searchParams.has('area')
    ? searchParams.get('area') === 'bitkub-city-mutelulu'
      ? 'bkcity'
      : 'dungeon'
    : 'bkcity';
  const types = searchParams.get('type')?.split(',') || [
    'living-house',
    'my-land',
    'land',
    'living-house',
  ];

  const [selectBuilding, setSelectBuilding] = useState<
    ILiving | IConcession | null
  >(null);

  const { data: session } = useSession();

  const { data: myarea } = useQuery({
    queryKey: ['myarea', session?.user?.wallet_address, city],
    queryFn: () =>
      areaApi.getMyArea({
        walletAddress: session?.user?.wallet_address as Address,
      }),
    enabled: !!session?.user?.wallet_address,
  });

  const myAreaGeojson = useMemo(() => {
    const sizes = searchParams.has('size')
      ? searchParams.get('size')?.split(',')
      : ['xs', 's', 'm', 'l', 'xl'];
    const features = myarea?.dungeon
      ?.filter((item) => sizes?.includes(item.size.toLowerCase()))
      .map((item) =>
        rectangleToGeoJSONPolygon(item.x, item.y, item.w, item.d, item.id)
      );
    return features || [];
  }, [myarea, searchParams]);

  return (
    <>
      {!selectBuilding?.building_id ? (
        <div className='absolute top-6 z-[2000] flex space-x-1'>
          <ConcessionLivingSelect
            value={selectBuilding?.building_id}
            onValueChange={(value) => {
              setSelectBuilding(value);
            }}
          />
        </div>
      ) : null}
      <MapContainer
        id='dungeon'
        center={center}
        zoom={MIN_ZOOM + 1}
        scrollWheelZoom={true}
        className='w-screen h-[calc(100dvh-64px)] bg-transparent'
        zoomControl={false}
      >
        <TileLayer
          noWrap={true}
          maxZoom={MAX_ZOOM}
          minZoom={MIN_ZOOM}
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">XRB Dungeon</a> contributors'
          url='dungeon/{z}/{x}/{y}.png'
        />
        <ZoomControl position='bottomleft' />
        <Suspense fallback={<Loader className='animate-spin' />}>
          {selectBuilding ? (
            <PlaceBuilding
              transformX={TRANSFROM_X}
              transformY={TRANSFROM_Y}
              myArea={myAreaGeojson}
              builging={selectBuilding}
              onCancel={() => {
                setSelectBuilding(null);
              }}
            />
          ) : null}
        </Suspense>
        <LayersControl>
          <LayersControl.BaseLayer
            checked={types.includes('land')}
            name='Landsale'
          >
            <DungeonLandsale
              transformX={TRANSFROM_X}
              transformY={TRANSFROM_Y}
            />
          </LayersControl.BaseLayer>
          <LayersControl.Overlay
            checked={types.includes('my-land')}
            name='My area'
          >
            <MyArea transformX={TRANSFROM_X} transformY={TRANSFROM_Y} />
          </LayersControl.Overlay>
          <LayersControl.Overlay
            checked={types.includes('living-house')}
            name='My building'
          >
            <MyBuildingOverlay
              transformX={TRANSFROM_X}
              transformY={TRANSFROM_Y}
            />
          </LayersControl.Overlay>
        </LayersControl>
      </MapContainer>
    </>
  );
};

export default Dungeon;
