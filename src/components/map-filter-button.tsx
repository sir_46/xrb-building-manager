'use client';
import { useCallback, useRef, useState } from 'react';
import { Button } from './ui/button';
import { ChevronLeft, ListFilter } from 'lucide-react';
import { cn } from '@/lib/utils';
import LandTypeCheckBox from './land-type-checkbox';
import LandSizeCheckbox from './land-size-checkbox';
import AreaRadio from './area-radio';

import {
  Drawer,
  DrawerTrigger,
  DrawerContent,
  DrawerHeader,
} from './ui/drawer';
import { useOnClickOutside } from 'usehooks-ts';
import { useMediaQuery } from '@/hooks/useMediaQuery';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { Label } from './ui/label';

type Props = {};

const MapFilterButton = (props: Props) => {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();
  const ref = useRef(null);
  const [open, setOpen] = useState<boolean>(false);

  const isDesktop = useMediaQuery('(min-width: 768px)');
  useOnClickOutside(ref, () => setOpen(false));

  const toggle = useCallback(() => setOpen((prev) => !prev), []);

  const TrigerButton = useCallback(() => {
    return (
      <Button
        size='icon'
        variant='outline'
        className='rounded-full'
        onClick={toggle}
      >
        {open ? (
          <ChevronLeft className='size-5' />
        ) : (
          <ListFilter className='size-5' />
        )}
      </Button>
    );
  }, [toggle, open]);

  const FilterForm = useCallback(() => {
    return (
      <>
        <Label>Area</Label>
        <AreaRadio
          onChange={(e) => {
            const params = new URLSearchParams(searchParams);
            params.set('area', e);
            router.push(`${pathname}?${params}`);
          }}
          value={searchParams.get('area') || 'bitkub-city-mutelulu'}
        />
        <Label>Type</Label>
        <LandTypeCheckBox
          onChange={(value) => {
            const params = new URLSearchParams(searchParams);
            params.set('type', value.join());
            router.push(`${pathname}?${params}`);
          }}
        />
        <Label>Size</Label>
        <LandSizeCheckbox
          onChange={(value) => {
            const params = new URLSearchParams(searchParams);
            params.set('size', value.join());
            router.push(`${pathname}?${params}`);
          }}
        />
      </>
    );
  }, [searchParams, router, pathname]);

  if (isDesktop) {
    return (
      <div
        ref={ref}
        className={cn(
          'absolute top-8 transition-all',
          open ? 'left-4' : '-left-60'
        )}
      >
        <div className='flex flex-row-reverse gap-2'>
          <TrigerButton />
          <div
            className={cn(
              'flex flex-col gap-4 w-60 rounded px-4 py-5 bg-background border'
            )}
          >
            <FilterForm />
          </div>
        </div>
      </div>
    );
  }

  return (
    <Drawer open={open} onOpenChange={setOpen}>
      <DrawerTrigger asChild>
        <div className='absolute top-8 left-4'>
          <TrigerButton />
        </div>
      </DrawerTrigger>
      <DrawerContent>
        <DrawerHeader className='text-left'>
          <FilterForm />
        </DrawerHeader>
      </DrawerContent>
    </Drawer>
  );
};

export default MapFilterButton;
