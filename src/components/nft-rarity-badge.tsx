import React from 'react';
import { Badge } from './ui/badge';
import { Rarity } from '@/types';

type Props = {
  value: Rarity;
};

const NFTRarityBadge = ({ value }: Props) => {
  if (value === 'common') return null;
  return (
    <Badge variant={value} className='capitalize'>
      {value}
    </Badge>
  );
};

export default NFTRarityBadge;
