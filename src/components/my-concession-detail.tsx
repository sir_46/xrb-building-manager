'use client';
import {
  useMutation,
  useQueries,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query';
import { useParams, useRouter } from 'next/navigation';

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from './ui/accordion';
import { AspectRatio } from './ui/aspect-ratio';
import Image from 'next/image';
import { ArrowLeftIcon, PlusIcon } from '@radix-ui/react-icons';
import Link from 'next/link';
import { RadioGroup, RadioGroupItem } from './ui/radio-group';
import { Label } from './ui/label';
import { Button } from './ui/button';
import concessionApi from '@/services/concessionApi';
import { useCallback, useState } from 'react';
import { Check, Loader } from 'lucide-react';
import NFTDeleteButton from './nft-delete-button';
import doorApi, { DoorSettingProps } from '@/services/doorApi';
import { DoorType } from '@/types';
import { Address } from 'viem';
import SkeletonWrapper from './skeleton-wrapper';
import { toast } from 'sonner';
import PasscodeDialog from './passcode-dialog';
import NFTEditButton from './nft-edit-button';
import { imageSizes } from '@/lib/utils';
import { useSession } from 'next-auth/react';

const DOOR_SETTING_OPTIONS = [
  {
    id: 'OPEN',
    label: 'Open',
  },
  {
    id: 'ONLY_ME',
    label: 'Only me',
  },
  {
    id: 'ONLY_FRIEND',
    label: 'Only friends',
  },
  {
    id: 'PASSCODE',
    label: 'Passcode',
  },
] as const;

type Props = {};

const MyConcessionDetail = (props: Props) => {
  const params = useParams();
  const router = useRouter();
  const buildingId = params.id;

  const queryClient = useQueryClient();

  const [door, setDoor] = useState<DoorType | null>(null);

  const [passcode, setPasscode] = useState<string | null>(null);
  const { data: session } = useSession();

  const { data, isLoading } = useQuery({
    queryKey: ['my-concession', buildingId],
    queryFn: () =>
      concessionApi.fetchConcessionDetails({ buildingId: String(buildingId) }),
    enabled: !!buildingId,
    select(data) {
      return data;
    },
  });

  const getNFT = useCallback(
    (index: number) => {
      return data?.nfts![index];
    },
    [data]
  );

  const doorMutation = useMutation({
    mutationKey: ['door-seeting', buildingId],
    mutationFn: doorApi.updateDoorSettings,
    onMutate() {
      toast.loading('Updating door settings...', {
        id: 'door-setting',
      });
    },
    onSuccess() {
      toast.success('Door settings have been successfully updated.', {
        id: 'door-setting',
      });
      setDoor(null);
      queryClient.refetchQueries({ queryKey: ['my-concession', buildingId] });
    },
    onError(error) {
      toast.error('Failed to update door settings.', {
        description: error.message,
        id: 'door-setting',
      });
    },
  });

  return (
    <>
      <PasscodeDialog
        open={door === 'PASSCODE' && !passcode}
        onChange={(e) => setPasscode(e)}
        onCancel={() => setDoor(null)}
      />
      <div className='flex items-center space-x-2'>
        <Button size='icon' variant='ghost' onClick={() => router.back()}>
          <ArrowLeftIcon />
        </Button>
        <SkeletonWrapper isLoading={isLoading}>
          <h1 className='text-xl font-black min-h-7 min-w-28 line-clamp-1'>
            {data?.name_en}
          </h1>
        </SkeletonWrapper>
      </div>
      <SkeletonWrapper isLoading={isLoading}>
        <Accordion
          type='single'
          defaultValue='nft'
          collapsible
          className='w-full bg-secondary/30 rounded overflow-hidden mt-2'
        >
          <AccordionItem value='nft' className='px-4'>
            <AccordionTrigger>
              <p>
                NFT Frame
                <span className='text-muted-foreground ml-1'>
                  ({data?.nfts?.length}/{data?.frame})
                </span>{' '}
              </p>
            </AccordionTrigger>
            <AccordionContent>
              <div className='overflow-auto'>
                <ul className='flex space-x-4 w-fit'>
                  {new Array(data?.frame).fill('').map((_, idx) => {
                    const nft = getNFT(idx);

                    return (
                      <li className='m-auto' key={idx}>
                        {nft ? (
                          <div className='relative group border rounded overflow-hidden size-28'>
                            <div className='absolute space-x-2 invisible group-hover:visible translate-x-[calc(50%-20px)] translate-y-[calc(50%+18px)] z-10'>
                              <NFTDeleteButton id={nft.id} />
                              <NFTEditButton data={nft} />
                            </div>
                            <AspectRatio ratio={1 / 1}>
                              <Image
                                src={nft.image}
                                fill
                                sizes={imageSizes}
                                className='object-contain'
                                alt={nft.nft_name + nft.token_id}
                              />
                            </AspectRatio>
                          </div>
                        ) : (
                          <Link
                            href={`/my-concession/${buildingId}/nfts`}
                            className='size-28 grid place-content-center bg-background rounded justify-center border border-dashed cursor-pointer'
                          >
                            <PlusIcon />
                          </Link>
                        )}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value='door-setting' className='px-4'>
            <AccordionTrigger>Door Setting</AccordionTrigger>
            <AccordionContent>
              <div className='flex h-max space-x-2'>
                <RadioGroup
                  value={door || data?.door}
                  className='flex flex-col sm:flex-row space-x-0 sm:space-x-4 min-h-8'
                  onValueChange={(e: DoorType) => {
                    if (e === data?.door) {
                      setDoor(null);
                      return;
                    } else {
                      setDoor(e);
                    }
                  }}
                >
                  {DOOR_SETTING_OPTIONS.map((opt) => (
                    <div key={opt.id} className='flex items-center space-x-2'>
                      <RadioGroupItem value={opt.id} id={opt.id} />
                      <Label htmlFor={opt.id}>{opt.label}</Label>
                    </div>
                  ))}
                </RadioGroup>
                {door ? (
                  <Button
                    size='sm'
                    className='space-x-2'
                    onClick={() => {
                      if (!data) {
                        return;
                      }

                      const payload: DoorSettingProps = {
                        token_id: String(buildingId),
                        wallet_address: session?.user
                          ?.wallet_address as Address,
                        owner_id: session?.user?.client_id,
                        open_type: door, //OPEN, ONLY_ME, ONLY_FRIEND,PASSCODE
                        passcode: passcode || undefined,
                      };

                      doorMutation.mutate(payload);
                    }}
                  >
                    {doorMutation.isPending ? (
                      <Loader className='size-4 animate-spin' />
                    ) : (
                      <Check className='size-4' />
                    )}
                    <span>Save</span>
                  </Button>
                ) : null}
              </div>
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value='top-view' className=' px-4 border-b-0'>
            <AccordionTrigger>Floor Plan</AccordionTrigger>
            <AccordionContent>
              {data?.floor_plan_path ? (
                <AspectRatio ratio={16 / 9}>
                  <Image
                    fill
                    sizes={imageSizes}
                    src={`${process.env.NEXT_PUBLIC_IMAGE_HOST}${data.floor_plan_path}`}
                    alt={data?.name_en}
                    className='object-contain'
                  />
                </AspectRatio>
              ) : (
                <div className='w-full flex items-center flex-col space-y-2'>
                  <p className='text-muted-foreground'>Empty</p>
                </div>
              )}
            </AccordionContent>
          </AccordionItem>
        </Accordion>
      </SkeletonWrapper>
    </>
  );
};

export default MyConcessionDetail;
