'use client';
import { useCallback, useEffect, useState } from 'react';
import { Button } from './ui/button';
import { HamburgerMenuIcon } from '@radix-ui/react-icons';
import { Sheet, SheetContent, SheetTrigger } from './ui/sheet';
import Logo from './logo';
import Menus from './menus';

type Props = {};

const HamburgerButton = (props: Props) => {
  const [open, setOpen] = useState<boolean>(false);

  const toggle = useCallback(() => {
    setOpen(!open);
  }, [open]);

  return (
    <Sheet open={open} onOpenChange={setOpen}>
      <SheetTrigger asChild>
        <Button
          size='icon'
          variant='ghost'
          className='flex md:hidden'
          onClick={toggle}
        >
          <HamburgerMenuIcon />
        </Button>
      </SheetTrigger>
      <SheetContent side='left'>
        <>
          <Logo className='h-7' />
          <Menus onClick={toggle} className='mt-4' />
        </>
      </SheetContent>
    </Sheet>
  );
};

export default HamburgerButton;
