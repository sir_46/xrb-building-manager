import { axiosInstance } from './../lib/axiosInstance';
import { IConcession } from '@/types';
import { Address } from 'viem';

export type ConcesssionFilterType = 'building' | 'concession' | 'all';

async function fetchConcessions(): Promise<Array<IConcession>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/concession/list`;
  return axiosInstance.get(URL).then((res) => res.data.list);
}

async function fetchMyConcessions({
  walletAddress,
  filter,
}: {
  walletAddress: Address;
  filter: ConcesssionFilterType;
}) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/my-concession/list/${walletAddress}?type=${filter}`;
  return await axiosInstance
    .get(URL)
    .then((res) => res.data.list as Array<IConcession>);
}

async function fetchConcessionDetails({ buildingId }: { buildingId: string }) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/my-concession/detail/${buildingId}`;
  return await axiosInstance
    .get(URL)
    .then((res) => res.data.list as IConcession);
}

const concessionApi = {
  fetchConcessions,
  fetchMyConcessions,
  fetchConcessionDetails,
};

export default concessionApi;
