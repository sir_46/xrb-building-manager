import { axiosInstance } from '@/lib/axiosInstance';
import { ILiving } from '@/types';

async function fetchLivingList(): Promise<Array<ILiving>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/living/list`;
  return axiosInstance.get(URL).then((res) => res.data.list);
}

const livingApi = {
  fetchLivingList,
};
export default livingApi;
