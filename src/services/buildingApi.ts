import { axiosInstance } from '@/lib/axiosInstance';
import { IBuilding, IConcession, ILiving } from '@/types';

async function fetchPlacedBuildings(): Promise<Array<IBuilding>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/building/list`;
  return axiosInstance.get(URL).then((res) => res.data.list);
}

async function fetchMyBuildings({
  city,
  walletAddress,
}: {
  city: 'bkcity' | 'metelulu' | 'duncity';
  walletAddress: string;
}): Promise<{
  living: ILiving[];
  concession: IConcession[];
}> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/my/building/${city}/${walletAddress}`;
  return await axiosInstance.get(URL).then((res) => res.data);
}

export interface CreateBuildingProps {
  wallet_address: String;
  building_id: string;
  area_id: Array<String>;
  pos_x: number;
  pos_y: number;
  pos_z: number;
  rot_z: number;
  city: 'BKCITY' | 'DUNCITY';
  category: 'Concession' | 'Living';
}

async function createBuilding(
  payload: CreateBuildingProps
): Promise<any | undefined> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/building/create`;
  return await axiosInstance.post(URL, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export interface UpdateBuildingProps extends CreateBuildingProps {
  id: number;
  token_id: string;
}

async function updateBuilding(
  payload: UpdateBuildingProps
): Promise<Array<IBuilding>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/building/edit`;
  return await axiosInstance.post(URL, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export interface DeleteBuildingProps {
  token_id: string;
}

async function deleteBuilding(
  payload: DeleteBuildingProps
): Promise<Array<IBuilding>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/building/delete`;
  return await axiosInstance.post(URL, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

const buildingApi = {
  fetchMyBuildings,
  fetchPlacedBuildings,
  createBuilding,
  updateBuilding,
  deleteBuilding,
};

export default buildingApi;
