import { axiosInstance } from '@/lib/axiosInstance';
import { LandSaleBKCity } from '@/types';

async function fetchLandSales({
  variant,
}: {
  variant: 'bkcity' | 'dungeon';
}): Promise<Array<LandSaleBKCity>> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/landsales/${variant}`;
  return axiosInstance.get(URL).then((res) => res.data.list);
}

const landsaleApi = {
  fetchLandSales,
};

export default landsaleApi;
