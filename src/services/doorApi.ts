import { axiosInstance } from '@/lib/axiosInstance';
import { DoorType } from '@/types';
import { AxiosRequestConfig } from 'axios';
import { Address } from 'viem';

export interface DoorSettingProps {
  token_id: string;
  wallet_address: Address;
  owner_id?: string;
  open_type: DoorType; //OPEN, ONLY_ME, ONLY_FRIEND,PASSCODE
  passcode?: string;
}

function updateDoorSettings(data: DoorSettingProps) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/door/setting`;
  const config: AxiosRequestConfig = {
    url: URL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };
  return axiosInstance.request(config);
}

const doorApi = {
  updateDoorSettings,
};

export default doorApi;
