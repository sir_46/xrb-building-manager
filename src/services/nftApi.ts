import { axiosInstance } from './../lib/axiosInstance';
import axios, { AxiosRequestConfig } from 'axios';

export interface NFTCreateType {
  project_name: string;
  nft_name: string;
  contract_address: string;
  token_id: string;
  building_id: string;
  image: string;
}

function createNFT(data: NFTCreateType) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/nft/create`;
  const config: AxiosRequestConfig = {
    url: URL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };
  return axiosInstance.request(config);
}

export interface NFTUpdateType extends NFTCreateType {
  id: string;
}
function updateNFT(data: NFTUpdateType) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/nft/edit`;
  const config: AxiosRequestConfig = {
    url: URL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };
  return axiosInstance.request(config);
}

function deleteNFT(data: { id: number }) {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/nft/delete`;
  const config: AxiosRequestConfig = {
    url: URL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };
  return axiosInstance.request(config);
}

const nftAPI = {
  createNFT,
  updateNFT,
  deleteNFT,
};

export default nftAPI;
