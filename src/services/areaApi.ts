import { Address } from 'viem';
import { Bkcity, Dungeon } from '@/types';
import { axiosInstance } from '@/lib/axiosInstance';

type Response = {
  bkcity: Array<Bkcity>;
  dungeon: Array<Dungeon>;
};
type Props = {
  walletAddress: Address;
};

async function getMyArea({ walletAddress }: Props): Promise<Response> {
  const URL = `${process.env.NEXT_PUBLIC_API_HOST}/building-manager/myarea/${walletAddress}`;
  return axiosInstance.get(URL).then((res) => res.data.list);
}

const areaApi = {
  getMyArea,
};

export default areaApi;
