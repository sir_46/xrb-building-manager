import { Address } from 'viem';

export interface IUser {
  id: string;
  display_name?: any;
  username: string;
  phone: string;
  email: string;
  lang: string;
  status: string;
  has_migrade: boolean;
  has_email: boolean;
  has_pincode: boolean;
  has_password: boolean;
  kyc_level: number;
  accepted_term_and_condition?: any;
  client_id: string;
  wallet_address: string;
  scope?: any;
  latest_term_and_condition: string;
  iat: number;
  exp: number;
  aud: string;
  iss: string;
  sub: string;
  jwt_auth: boolean;
  developer_mode: boolean;
  created_time: number;
}

export type Rarity = 'uncommon' | 'common' | 'rare';

export interface Bkcity {
  id: string;
  size: string;
  area: string;
  x: number;
  y: number;
  w: number;
  d: number;
}

export interface Dungeon {
  dunland_id: number;
  id: string;
  size: string;
  area: string;
  x: number;
  y: number;
  w: number;
  d: number;
}

type CategoryType = 'building' | 'concession';

export interface IConcession {
  building_id: string;
  size: string;
  name_th: string;
  name_en: string;
  link_concession: string;
  icon_name: string;
  topview_name: string;
  floor_plan_name: string;
  created_at: string;
  frame: number;
  w: number;
  d: number;
  id: number;
  topview90_name: string;
  topview90_noco_path?: string;
  topview180_name: string;
  topview180_noco_path?: string;
  topview270_name: string;
  topview270_noco_path?: string;
  item_count: string;
  icon_path: string;
  topview_path: string;
  floor_plan_path: string;
  door?: string;
  nfts?: Nft[];
  topview90_path?: string;
  topview180_path?: string;
  topview270_path?: string;
  enable: boolean;
  category: CategoryType;
}

export interface IBuilding {
  token_id: string;
  wallet_address: string;
  pos_x: number;
  pos_y: number;
  pos_z: number;
  rot_z: number;
  category: string;
  name_th: string;
  name_en: string;
  size: string;
  icon_name: string;
  topview_name: string;
  w: number;
  d: number;
  area: string;
  icon_path: string;
  topview_path: string;
  topview90_path: string;
  topview180_path: string;
  topview270_path: string;
  city: string;
}

export interface LandSaleBKCity {
  dunland_id: number;
  id: string;
  size: string;
  area: string;
  x: number;
  y: number;
  w: number;
  d: number;
}

export interface ILiving {
  building_id: string;
  size: string;
  name_th: string;
  name_en: string;
  link_concession: string;
  icon_name: string;
  topview_name: string;
  floor_plan_name: string;
  created_at: string;
  frame: number;
  w: number;
  d: number;
  id: number;
  topview90_name: string;
  topview180_name: string;
  topview270_name: string;
  item_count: string;
  icon_path: string;
  topview_path: string;
  topview90_path: string;
  topview180_path: string;
  topview270_path: string;
  floor_plan_path: string;
  enable: boolean;
  category: CategoryType;
}

export interface NFTToken {
  name: string;
  address: Address;
  image: string;
  type: string;
  bit_offset?: number;
  order: number;
  disable_transfer?: boolean;
  available_set_profile?: boolean;
  nfts?: Nft[];
}

export interface Nft {
  name: string;
  address: string;
  image: string;
  type: string;
  bit_offset: number;
  order: number;
  disable_transfer: boolean;
  available_set_profile: boolean;
  id: number;
  project_name: string;
  nft_name: string;
  contract_address: string;
  token_id: string;
  building_id: string;
  created_at: string;
  updated_at: string;
}

export interface IPFS {
  name: string;
  description: string;
  image: string;
  thumbnail_image: string;
  attributes: Attribute[];
  hashtags: string[];
}

export interface Attribute {
  trait_type: string;
  value: string;
}

export type DoorType = 'OPEN' | 'ONLY_ME' | 'ONLY_FRIEND' | 'PASSCODE';
