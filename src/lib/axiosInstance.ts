import axios from 'axios';
import { getCookie } from 'cookies-next';

export const axiosInstance = axios.create({});

axiosInstance.interceptors.request.use(
  (config) => {
    const token = getCookie('access_token'); // Retrieve auth token from cookie
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    const originalConfig = err.config;

    if (err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;

        try {
          const refreshToken = localStorage.getItem('refresh_token');
          if (refreshToken) {
            const response = await fetch('/user/info', {
              method: 'post',
              body: JSON.stringify({
                refreshToken: refreshToken,
              }),
            });

            // const response = await refreshAccessToken({
            //   refreshToken: refreshToken,
            // });
            // const { access_token, refresh_token, expires_in } = response;
            // localStorage.setItem('access_token', access_token);
            // localStorage.setItem('refresh_token', refresh_token);
            // localStorage.setItem('expires_in', `${expires_in}`);
            // localStorage.setItem('updated_at', `${new Date()}`);
          }

          return axiosInstance(originalConfig);
        } catch (_error: any) {
          if (_error.response && _error.response.data) {
            return Promise.reject(_error.response.data);
          }

          return Promise.reject(_error);
        }
      }

      if (err.response.status === 403 && err.response.data) {
        return Promise.reject(err.response.data);
      }
    }

    return Promise.reject(err);
  }
);
