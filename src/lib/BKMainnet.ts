import { createPublicClient, http } from 'viem';
import { BKChain } from './BKChain';

export const BKMainnet = createPublicClient({
  chain: BKChain,
  transport: http(),
});
