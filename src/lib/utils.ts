import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';
import { BKMainnet } from './BKMainnet';
import { ticketAbi } from './abi/tikcetAbi';
import { Address } from 'viem';
import { NFTToken } from '@/types';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const KKUB_ADDRESS = '0x37f57ae5c5662191a5d87d94c8a8445ee2e2aba7';
export function convertToKebabCase(input: string): string {
  return input
    .toLowerCase() // Convert the string to lowercase
    .replace(/[^a-z0-9]+/g, '-') // Replace non-alphanumeric characters with hyphens
    .replace(/^-+|-+$/g, ''); // Remove leading and trailing hyphens
}

export function convertFromKebabCase(input: string): string {
  return input
    .split('-') // Split the string by hyphens
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1)) // Capitalize the first letter of each word
    .join(' '); // Join the words with spaces
}

export const onConnectBKN = async () => {
  const windowFeatures = 'left=100,top=100,width=420,height=620';
  const client_id = process.env.NEXT_PUBLIC_BK_CLIENT_ID;
  const redirect_uri = process.env.NEXT_PUBLIC_BK_REDIRECT_URL;
  const query_Parameters = `response_type=code&client_id=${client_id}&redirect_uri=${redirect_uri}`;
  const url = `${process.env.NEXT_PUBLIC_BKNEXT_URL}/oauth2/authorize?${query_Parameters}`;

  window.open(url, 'mozillaTab', windowFeatures);
};

export const truncatedAddress = (wallet: string) =>
  `${wallet.substring(0, 6)}...${wallet.substring(wallet.length - 5)}`;

export function removeStorageGoogleapis(url: string): string {
  const prefix = 'https://storage.googleapis.com/static.bitkubnext.com';
  if (url.startsWith(prefix)) {
    return url.replace(prefix, 'https://static.bitkubnext.com');
  }
  return url;
}

export const getTopviewPath = (rotation: number) => {
  switch (rotation) {
    case 0:
      return 'topview_path';
    case 90:
      return 'topview90_path';
    case 180:
      return 'topview180_path';
    case 270:
      return 'topview270_path';
    default:
      return 'topview_path';
  }
};

/**
 * Converts a value in Ether to Wei.
 * @param ether - The value in Ether as a string or BigInt.
 * @returns The value in Wei as a BigInt.
 */
export function etherToWei(ether: string | bigint): bigint {
  const weiPerEther = BigInt(10 ** 18);
  return BigInt(ether) * weiPerEther;
}

export function truncatedNFTToken(input: string) {
  if (input.length < 10) {
    return input;
  }
  return `${input.substring(0, 4)}...${input.substring(input.length - 4)}`;
}

// Function to fetch balance
export const fetchBalance = async (address: string, walletAddress: string) => {
  return BKMainnet.readContract({
    address: address as Address,
    abi: ticketAbi,
    functionName: 'balanceOf',
    args: [walletAddress as Address],
  });
};

export const imageSizes =
  '(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw';

export const fallbackImage =
  'data:image/webp;base64,UklGRsYGAABXRUJQVlA4ILoGAACQbgCdASqAAoACPpFIokylpKMiIPgoALASCWlu4XdhG/Nj8i/2jtT/tuQrANfaGIocUOL1XyNX0JM+X030zPsd9IAC3B7yb6dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnRwlubeMVCcEIis0Xo9ANkOimMyF7hivl7jT3k306dOnTp06b6jgOasuA5xn3ppIJpQDgHD8Oclo5CgRzG9rOnTp06dOnTp06dOm+tOj91rEZTy6AGmLYNdycPGX0XZV8vcae8m+nTp06dOjoqtLguL6R2MUEaB577R9ihZwPFlAPeTfTp06dOnTp06dOjhpbtr4yEstCWyofrHuTp06dOnTp06dOnTp06b41FLTUoKIBzRG5hLBKCXuNPeTfTp06dOnTp06dN9WM7qWFEhRuNPeTfTp06dOnTp06dOnTfH6UEADN0VJvp06dOnTp06dOnTp06dOm+JeMm2+gy3jsYOr9OnTp06dOnTp06dOnTp03x2fcEl2g4eCishPeTfTp06dOnTp06dOnTpvzUbb1FV8vcae8m+nTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp06dOnTp0cAAD+/9SwAAAAAAAAAAAAAAAAAAABSLLVaYy7l18ydEtzeT848TzaN3lmf4gQlQHrYBZF3Txsc2vOqU+mGoflmj3FG1KiIg75nXE36zjYVfUKkWe3h5wRuC5FL/fU+HX4wkDOYq2g0ES4nssnmbYw+hMX3R4wRnlMEyowdmzKpeEBFFNFGoj5tHAew47iq952DNmucn+0a29Do8fMfafDry7/5xNpfksubZs4A9rRSlornwaJRHdUG7Nxxf84Kv3F0jItYFS0SEi8b7SApqgz8+aOZm2PN2y2dqjH4hR/t9YK1jZ5mHbK16LeXMzVZ9gulNbeiH7eBLvIVlyGB8t3rH3ZTfOQqQOi4mmSmPI8faA9sR7ABbqg9PCtN+GR4mN1rTxrRRq4Yt2EuX6TtpJTa5dIXCLlmvU4JvEnld2Lynia3ssaKtRcCi+UcT/qt7RXBX3bN/hMTAQRQYWBkEoCMc58CZyTtMbAauggBawndUPWu1E5aO+vF1ozKIjzlgdANKqu8RG6no9LLzbjOZXesdEYPpybN79t0Ev/0QVN/A+AHnlEemU9Ck8keuQNdY4Sj6lZASy3L6NBPXO4NXZBtzU+ANTf/aMe2Au6OgkQNVz4pukHpLwqcbyk+3kEYa1W+2KSrvgdsISn3POTtHpEpDMftVx94jBuHmVE1eSH7r7c2WWrA160o9+hXnsVM1HD+IkjS2vabmkAaZzex0vL+zeWpmZY9fGfrcrQDtihvgH8QcJ0U1yeyum6qWNPbM+TLV+09PIRaN8ulSEwXwykLLdzPhCoTKOoe0rqhZDiIwM2G8ka5NfMOQ8E79ho2S/FqH+HHxKsG+pAAG7i/o7fY3r3jYbV67RIwI6R8bQxSdfo8ARJPtZFR9amXwrUcfkgAZICuvFx7bU4ybi60xj+ci3aYhOj4nmpV4Rf2o8luap5J5WIodldIe+IAg/8Ak9DEY+pkcR3rztCPstbYVi6CmY/Lk7I4+Dy46mejFSwfVtnEL5Z9ncAUBOQ07E4BQGVmAxtJ/0PneGxn6l23T64q0dCE3L0b/e9Jb7WUAkDFyoqvsbh2vwNpZkaByVgAAAAAAAAAAAAAAAAAAAAAAA=';

export function capitalizeFirstLetter(str: string) {
  if (!str) return str;
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function extractAddresses(nftTokens: Array<NFTToken>): Address[] {
  let addresses: string[] = [];

  nftTokens.forEach((nftToken) => {
    addresses.push(nftToken.address);
    if (nftToken.nfts) {
      addresses = addresses.concat(nftToken.nfts.map((nft) => nft.address));
    }
  });

  return addresses.filter((address) => address as Address) as Address[];
}

export function mergeArrayOfRecords(
  array: Array<Record<string, number>>
): Record<string, number> {
  return array.reduce((accumulator, current) => {
    return { ...accumulator, ...current };
  }, {});
}
