import { IUser } from '@/types';
export async function useServerSession(): Promise<IUser | undefined> {
  'use server';
  try {
    const res = await fetch('/api/user');
    const data = await res.json();

    console.log('useServerSession', data);

    return data;
  } catch (error) {
    return undefined;
  }
}
