import { IUser } from '@/types';
import { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { cookies } from 'next/headers';

export const authOptions: NextAuthOptions = {
  session: {
    strategy: 'jwt',
    maxAge: 1 * 60 * 60, // 1 hour
  },
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        code: {
          label: 'code',
          type: 'string',
        },
      },
      async authorize(credentials, req) {
        try {
          const { code } = credentials as { code: string };

          const resToken = await requestAccessToken(code);
          if (resToken.ok) {
            const { access_token, refresh_token, expires_in } =
              await resToken.json();

            cookies().set('access_token', access_token);
            cookies().set('refresh_token', refresh_token);
            cookies().set('expires_in', expires_in);
            const resUser = await getUser(access_token);

            if (resUser.ok) {
              const user = await resUser.json();
              return user.data as IUser;
            } else {
              console.error('Failed to fetch user data', resUser.statusText);
            }
          } else {
            console.error('Failed to fetch access token', resToken.statusText);
          }
        } catch (error) {
          console.error('Error in authorize function', error);
        }
        return null;
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        token.user = user;
      }
      return Promise.resolve(token); // JWT interface we declared in next-auth.d.ts
    },
    async session({ session, token }) {
      session.user = token.user;
      return session; // Session interface we declared in next-auth.d.ts
    },
  },
  secret: process.env.NEXTAUTH_SECRET,
  pages: {
    signIn: '/',
  },
};

export async function requestAccessToken(code: string) {
  const URL = `${process.env.NEXT_PUBLIC_BKNEXT_ACCESS_TOKEN_URL}`;

  const res = await fetch(URL, {
    method: 'post',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: [
      `grant_type=authorization_code`,
      `client_id=${process.env.BK_CLIENT_ID}`,
      `client_secret=${process.env.BK_CLIENT_SECRET}`,
      `redirect_uri=${process.env.NEXT_PUBLIC_BK_REDIRECT_URL}`,
      `code=${code}`,
    ].join('&'),
  });

  return res;
}

export async function getUser(accessToken: string) {
  const url = `${process.env.NEXT_PUBLIC_BKNEXT_API_URL}/accounts/auth/info`;

  const res = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
  });

  return res;
}
