import { defineChain } from 'viem';

export const BKChain = defineChain({
  id: 25925,
  name: 'Bitkub Mainnet',
  network: 'Bitkub Mainnet',
  nativeCurrency: {
    decimals: 18,
    name: 'Bitkub Mainnet',
    symbol: 'tKUB',
  },
  rpcUrls: {
    default: {
      http: ['https://rpc.bitkubchain.io'],
    },
    public: {
      http: ['https://rpc.bitkubchain.io'],
    },
  },
  blockExplorers: {
    default: { name: 'Explorer', url: 'https://bkcscan.com/' },
  },
  contracts: {
    multicall3: {
      address: '0x3c238cb1293c039aba3ab0f1840c8330bbd190eb',
      blockCreated: 13685238,
    },
  },
});
