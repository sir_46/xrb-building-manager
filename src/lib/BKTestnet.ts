import { createPublicClient, http } from 'viem';
import { BKChain } from './BKChain';

export const BKTestnet = createPublicClient({
  chain: BKChain,
  transport: http(),
});
